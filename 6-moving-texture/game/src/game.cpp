#include <iostream>

#include "engine/include/iengine.hpp"
#include "game/include/game.hpp"

game::game(iengine* engine)
    : engine_(engine)
{
}

bool game::init()
{
    if (!engine_->init(800,
                       600,
                       "resources/textures/red_dragon.png",
                       "resources/shaders/vertex.shader",
                       "resources/shaders/fragment.shader"))
    {
        std::cerr << "error: failed to init an engine" << std::endl;
        return false;
    }

    vertex v1 = { -0.3, 0.3, 0.0, 1.0 };
    vertex v2 = { 0.3, 0.3, 1.0, 1.0 };
    vertex v3 = { 0.3, -0.3, 1.0, 0.0 };
    vertex v4 = { -0.3, -0.3, 0.0, 0.0 };

    geometry_.push_back(v1);
    geometry_.push_back(v2);
    geometry_.push_back(v3);
    geometry_.push_back(v4);

    return true;
}

void game::shut_down()
{
    engine_->shut_down();
}

void game::on_action(action& action)
{
    // TODO: Calculate matrixes

    float step = 0.025f;

    move_matrix_[0][2] = 0.f;
    move_matrix_[1][2] = 0.f;

    if (action.pressed)
    {
        switch (action.act_type)
        {
            case action_type::UP:
                move_matrix_[1][2] += step;
                break;

            case action_type::BACK:
                move_matrix_[1][2] -= step;
                break;

            case action_type::LEFT:
                move_matrix_[0][2] -= step;
                break;

            case action_type::RIGHT:
                move_matrix_[0][2] += step;
                break;
        }
    }
}

void game::simulate()
{
    // TODO: Multiply to matrixes

    for (auto& vertex : geometry_)
    {
        float new_x = move_matrix_[0][0] * vertex.x +
                      move_matrix_[0][1] * vertex.y +
                      move_matrix_[0][2];
        float new_y = move_matrix_[1][0] * vertex.x +
                      move_matrix_[1][1] * vertex.y +
                      move_matrix_[1][2];

        vertex.x = new_x;
        vertex.y = new_y;
    }
}

void game::render()
{
    vertex v1 = geometry_[0];
    vertex v2 = geometry_[1];
    vertex v3 = geometry_[2];
    vertex v4 = geometry_[3];

    // BEGIN DEBUG OUTPUT

    //    std::cout << "v1.x = " << v1.x << "v1.y = " << v1.y << std::endl;
    //    std::cout << "v2.x = " << v2.x << "v2.y = " << v2.y << std::endl;
    //    std::cout << "v3.x = " << v3.x << "v3.y = " << v3.y << std::endl;
    //    std::cout << "v4.x = " << v4.x << "v4.y = " << v4.y << std::endl;

    // END DEBUG OUTPUT

    engine_->swap_buffers();
    engine_->clear_window(0, 0, 0);
    engine_->render_triangle(v1, v3, v4);
    engine_->render_triangle(v1, v2, v3);
}

igame* create_game(iengine* engine)
{
    auto g = new game(engine);
    return g;
}

void destroy_game(igame* game)
{
    delete game;
}