cmake_minimum_required(VERSION 3.17)

project(6-moving-texture CXX C)

set(CMAKE_CXX_STANDARD 17)

add_executable(engine
        engine/include/iengine.hpp
        engine/include/engine.hpp
        engine/include/engine_gl_debug.hpp
        engine/include/hot_loader.hpp

        engine/src/hot_loader.cpp
        engine/src/engine.cpp
        engine/main.cpp

        vendor/glad/glad.h
        vendor/KHR/khrplatform.h
        vendor/glad/glad.c

        vendor/stb/stb_image.hpp
        vendor/stb/stb_image.cpp)

add_library(game SHARED
        game/include/game.hpp
        game/src/game.cpp)

target_include_directories(engine PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories(game PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})

set_target_properties(engine PROPERTIES
        ENABLE_EXPORTS 1)

find_package(SDL2 REQUIRED)

target_link_libraries(engine PRIVATE
        SDL2::SDL2)
target_link_libraries(game PRIVATE
        engine)