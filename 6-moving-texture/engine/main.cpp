#include "engine/include/hot_loader.hpp"
#include "engine/include/iengine.hpp"

#include <cstdlib>
#include <iostream>

int main(int, char**)
{
    std::string game_lib_name = "game";
    hot_loader  hot_loader(game_lib_name);
    iengine*    engine = create_engine();
    igame* game = hot_loader.load_game(engine);

    if (!game->init())
    {
        std::cerr << "error: failed to init the game" << std::endl;
        return EXIT_FAILURE;
    }

    action action{};

    while (engine->process_input(&action))
    {
        //        if (hot_loader.is_game_updated())
        //        {
        //            game = hot_loader.load_game(engine);
        //        }

        game->on_action(action);
        game->simulate();
        game->render();
    }

    game->shut_down();
    hot_loader.unload_game();
    destroy_engine(engine);

    return EXIT_SUCCESS;
}