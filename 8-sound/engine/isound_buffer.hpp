#pragma once

class isound_buffer
{
public:
    virtual ~isound_buffer();

    virtual void play(bool looped) = 0;
};
