#include <iostream>
#include "greetings.h"

int sayHello()
{
  using namespace std;
  
  const string greeting = "Hello world!";
  cout << greeting << endl;

  return cout.good() ? 0 : 1;
}
