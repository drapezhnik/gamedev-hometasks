#pragma once

class iaudio_buffer
{
public:
    virtual ~iaudio_buffer();

    virtual void play(bool looped) = 0;
};
