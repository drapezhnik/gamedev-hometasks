#pragma once

#include "engine/iengine.hpp"
#include "input/action.hpp"

class igame
{
public:
    virtual ~igame() = default;

    virtual bool init()      = 0;
    virtual void shut_down() = 0;

    virtual void on_action(std::vector<action>& actions) = 0;
    virtual void simulate()                = 0;
    virtual void render()                  = 0;
};

extern "C"
{
    igame* create_game(iengine* engine);
    void   destroy_game(igame* game);
}