#pragma once

#include "audio/iaudio_buffer.hpp"
#include "input/action.hpp"
#include "render/triangle.hpp"
#include "render/vertex.hpp"

#include <cassert>
#include <cstdint>
#include <string_view>
#include <vector>

class iengine
{
public:
    virtual ~iengine() = default;

    // init / shut down
    virtual bool init(const std::string& game_name,
                      int                window_width,
                      int                window_height,
                      const std::string& path_to_vertex_shader_src,
                      const std::string& path_to_fragment_shader_src) = 0;
    virtual void shut_down()                                          = 0;

    // input
    virtual bool process_input(std::vector<action>* actions) = 0;

    // render
    virtual void swap_buffers()                                = 0;
    virtual void clear_window(uint8_t r, uint8_t g, uint8_t b) = 0;
    virtual void render(std::vector<vertex>&       vertexes,
                        std::vector<unsigned int>& indexes,
                        std::string&               path_to_texture)          = 0;

    virtual unsigned int get_milisec_count_from_init() = 0;

    // audio
    virtual iaudio_buffer* create_sound_buffer(std::string_view path) = 0;
    virtual void           destroy_sound_buffer(iaudio_buffer*)       = 0;
};

iengine* create_engine();
void     destroy_engine(iengine* engine);