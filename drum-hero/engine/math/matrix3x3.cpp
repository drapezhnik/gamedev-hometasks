#include "matrix3x3.hpp"

#include <cassert>

matrix3x3::matrix3x3()
{
    data_.resize(side_size * side_size);
    std::fill(data_.begin(), data_.end(), 0.f);

    // set main diagonal to 1
    data_.at(0 * side_size + 0) = 1.f;
    data_.at(1 * side_size + 1) = 1.f;
    data_.at(2 * side_size + 2) = 1.f;
}

float& matrix3x3::operator()(unsigned int row, unsigned int column)
{
    assert(row >= 0 && row < side_size);
    assert(column >= 0 && column < side_size);

    unsigned int index = row * side_size + column;
    return data_[index];
}

matrix3x3 operator*(matrix3x3 l_matrix, matrix3x3 r_matrix)
{
    matrix3x3 result;

    // clang-format off

    result(0, 0) =
        l_matrix(0,0) * r_matrix(0, 0) +
        l_matrix(0,1) * r_matrix(1, 0) +
        l_matrix(0,2) * r_matrix(2, 0);

    result(0, 1) =
        l_matrix(0,0) * r_matrix(0, 1) +
        l_matrix(0,1) * r_matrix(1, 1) +
        l_matrix(0,2) * r_matrix(2, 1);

    result(0, 2) =
        l_matrix(0,0) * r_matrix(0, 2) +
        l_matrix(0,1) * r_matrix(1, 2) +
        l_matrix(0,2) * r_matrix(2, 2);

    result(1, 0) =
        l_matrix(1,0) * r_matrix(0, 0) +
        l_matrix(1,1) * r_matrix(1, 0) +
        l_matrix(1,2) * r_matrix(2, 0);

    result(1, 1) =
        l_matrix(1,0) * r_matrix(0, 1) +
        l_matrix(1,1) * r_matrix(1, 1) +
        l_matrix(1,2) * r_matrix(2, 1);

    result(1, 2) =
        l_matrix(1,0) * r_matrix(0, 2) +
        l_matrix(1,1) * r_matrix(1, 2) +
        l_matrix(1,2) * r_matrix(2, 2);

    result(2, 0) =
        l_matrix(2,0) * r_matrix(0, 0) +
        l_matrix(2,1) * r_matrix(1, 0) +
        l_matrix(2,2) * r_matrix(2, 0);

    result(2, 1) =
        l_matrix(2,0) * r_matrix(0, 1) +
        l_matrix(2,1) * r_matrix(1, 1) +
        l_matrix(2,2) * r_matrix(2, 1);

    result(2, 2) =
        l_matrix(2,0) * r_matrix(0, 2) +
        l_matrix(2,1) * r_matrix(1, 2) +
        l_matrix(2,2) * r_matrix(2, 2);

    // clang-format off

    return result;
}
