#pragma once

#include "engine/math/matrix3x3.hpp"

class vector2
{
public:
    vector2() = default;
    vector2(float x, float y);

    float x = 0;
    float y = 0;
};

vector2 operator*(matrix3x3 matrix, vector2 vector);
vector2 interpolate (vector2 v1, vector2 v2, float amount);