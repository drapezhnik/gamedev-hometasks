#include "vector2.hpp"

vector2::vector2(float x, float y)
{
    this->x = x;
    this->y = y;
}

vector2 operator*(matrix3x3 matrix, vector2 vector)
{
    vector2 result;

    result.x = matrix(0, 0) * vector.x + matrix(0, 1) * vector.y + matrix(0, 2);
    result.y = matrix(1, 0) * vector.x + matrix(1, 1) * vector.y + matrix(1, 2);

    return result;
}

vector2 interpolate(vector2 v1, vector2 v2, float t)
{
    vector2 result;

    result.x = v1.x + (v2.x - v1.x) * t;
    result.y = v1.y + (v2.y - v1.y) * t;

    return result;
}
