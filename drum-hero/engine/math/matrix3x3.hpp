#pragma once

#include <vector>

class matrix3x3
{
private:
    int                side_size = 3;
    std::vector<float> data_;

public:
    matrix3x3();

    float&    operator()(unsigned int row, unsigned int column);
};

matrix3x3 operator*(matrix3x3 l_matrix, matrix3x3 r_matrix);