#include "engine.hpp"
#include "engine_gl_debug.hpp"
#include "render/renderer.hpp"
#include "vendor/glad/glad.h"
#include "vendor/stb/stb_image.hpp"

#include <array>
#include <engine/render/texture.hpp>
#include <iostream>
#include <vector>

bool engine::init(const std::string& game_name,
                  int                window_width,
                  int                window_height,
                  const std::string& path_to_vertex_shader_src,
                  const std::string& path_to_fragment_shader_src)
{
    using namespace std;
    const int sdl_init_result = SDL_Init(SDL_INIT_EVERYTHING);

    if (sdl_init_result != 0)
    {
        cerr << "error: SDL_Init failed: " << SDL_GetError() << endl;
        return false;
    }

    // set flag to enable debug gl context BEFORE sdl window init
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    window_ = SDL_CreateWindow(game_name.c_str(),
                               SDL_WINDOWPOS_CENTERED,
                               SDL_WINDOWPOS_CENTERED,
                               window_width,
                               window_height,
                               SDL_WINDOW_OPENGL);

    if (window_ == nullptr)
    {
        cerr << "error: SDL_CreateWindow failed: " << SDL_GetError() << endl;
        return false;
    }

    int gl_major_version   = 3;
    int gl_minor_version   = 2;
    int gl_context_profile = SDL_GL_CONTEXT_PROFILE_ES;

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_version);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_version);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, gl_context_profile);

    SDL_GLContext gl_context = SDL_GL_CreateContext(window_);

    if (gl_context == nullptr)
    {
        cerr << "error: SDL_GL_CreateContext failed: " << SDL_GetError()
             << endl;
        return false;
    }

    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_version);
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_version);

    clog << "OpenGL context created with version: " << gl_major_version << '.'
         << gl_minor_version << endl;

    if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0)
    {
        clog << "error: failed to initialize glad" << std::endl;
        return false;
    }

    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(callback_opengl_debug, nullptr);
    glDebugMessageControl(
        GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);

    path_to_vertex_shader_src_   = path_to_vertex_shader_src;
    path_to_fragment_shader_src_ = path_to_fragment_shader_src;

    init_audio();

    return true;
}

void engine::shut_down()
{
    SDL_DestroyWindow(window_);
    SDL_Quit();
}

bool engine::process_input(std::vector<action>* actions)
{
    bool result = input_processor_.process_input(actions);

    return result;
};

void engine::swap_buffers()
{
    SDL_GL_SwapWindow(window_);
}

void engine::clear_window(uint8_t r, uint8_t g, uint8_t b)
{
    renderer_.clear(r, g, b);
};

void engine::render(std::vector<vertex>&       vertexes,
                    std::vector<unsigned int>& indexes,
                    std::string&               path_to_texture)
{
    vertex_buffer vb(vertexes.data(), vertexes.size() * sizeof(vertex));
    index_buffer  ib(indexes.data(), indexes.size());

    vertex_array         va;
    vertex_buffer_layout layout;

    layout.push_float(2);
    layout.push_float(2);
    va.add_buffer(vb, layout);

    texture texture_(path_to_texture);
    texture_.bind();

    shader shader_(path_to_vertex_shader_src_, path_to_fragment_shader_src_);
    shader_.bind();
    shader_.set_uniform_1i("u_texture", 0);

    renderer_.draw(va, ib, shader_);
}

// sound
iaudio_buffer* engine::create_sound_buffer(std::string_view path)
{
    audio_buffer* s = new audio_buffer(path, audio_device, audio_device_spec);
    SDL_LockAudioDevice(audio_device); // TODO fix lock only push_back
    sounds.push_back(s);
    SDL_UnlockAudioDevice(audio_device);
    return s;
}

void engine::destroy_sound_buffer(iaudio_buffer* sound)
{
    // TODO FIXME first remove from sounds collection
    delete sound;
}

void engine::audio_callback(void* engine_ptr, uint8_t* stream, int stream_size)
{
    // no audio default
    std::fill_n(stream, stream_size, '\0');

    engine* e = static_cast<engine*>(engine_ptr);

    for (audio_buffer* snd : e->sounds)
    {
        if (snd->is_playing)
        {
            uint32_t rest         = snd->length - snd->current_index;
            uint8_t* current_buff = &snd->buffer[snd->current_index];

            if (rest <= static_cast<uint32_t>(stream_size))
            {
                // copy rest to buffer
                SDL_MixAudioFormat(stream,
                                   current_buff,
                                   e->audio_device_spec.format,
                                   rest,
                                   SDL_MIX_MAXVOLUME);
                snd->current_index += rest;
            }
            else
            {
                SDL_MixAudioFormat(stream,
                                   current_buff,
                                   e->audio_device_spec.format,
                                   static_cast<uint32_t>(stream_size),
                                   SDL_MIX_MAXVOLUME);
                snd->current_index += static_cast<uint32_t>(stream_size);
            }

            if (snd->current_index == snd->length)
            {
                if (snd->is_looped)
                {
                    // start from begining
                    snd->current_index = 0;
                }
                else
                {
                    snd->is_playing = false;
                }
            }
        }
    }
}

void engine::init_audio()
{
    audio_device_spec.freq     = 48000;
    audio_device_spec.format   = AUDIO_S16LSB;
    audio_device_spec.channels = 2;
    audio_device_spec.samples  = 1024; // must be power of 2
    audio_device_spec.callback = audio_callback;
    audio_device_spec.userdata = this;

    const int num_audio_drivers = SDL_GetNumAudioDrivers();
    for (int i = 0; i < num_audio_drivers; ++i)
    {
        std::cout << "audio_driver #:" << i << " " << SDL_GetAudioDriver(i)
                  << '\n';
    }
    std::cout << std::flush;

    // TODO on windows 10 only directsound - works for me
    if (std::string_view("Windows") == SDL_GetPlatform())
    {
        const char* selected_audio_driver = SDL_GetAudioDriver(1);
        std::cout << "selected_audio_driver: " << selected_audio_driver
                  << std::endl;

        if (0 != SDL_AudioInit(selected_audio_driver))
        {
            std::cout << "can't init SDL audio\n" << std::flush;
        }
    }

    const char* default_audio_device_name = nullptr;

    // SDL_FALSE - mean get only OUTPUT audio devices
    const int num_audio_devices = SDL_GetNumAudioDevices(SDL_FALSE);
    if (num_audio_devices > 0)
    {
        default_audio_device_name =
            SDL_GetAudioDeviceName(num_audio_devices - 1, SDL_FALSE);
        for (int i = 0; i < num_audio_devices; ++i)
        {
            std::cout << "audio device #" << i << ": "
                      << SDL_GetAudioDeviceName(i, SDL_FALSE) << '\n';
        }
    }
    std::cout << std::flush;

    audio_device = SDL_OpenAudioDevice(default_audio_device_name,
                                       0,
                                       &audio_device_spec,
                                       nullptr,
                                       SDL_AUDIO_ALLOW_ANY_CHANGE);

    if (audio_device == 0)
    {
        std::cerr << "failed open audio device: " << SDL_GetError();
        throw std::runtime_error("audio failed");
    }
    else
    {
        // unpause device
        SDL_PauseAudioDevice(audio_device, SDL_FALSE);
    }
}

iengine* create_engine()
{
    iengine* e = new engine();
    return e;
};

void destroy_engine(iengine* engine)
{
    delete engine;
};