#pragma once

class vertex
{
public:
    float x = 0.f;
    float y = 0.f;
    float u = 0.f; // texture coordinate x
    float v = 0.f; // texture coordinate y
};