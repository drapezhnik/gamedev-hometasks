#pragma once

#include "vendor/glad/glad.h"

#include <vector>

class vertex_buffer_element
{
public:
    unsigned int  type;
    unsigned int  count;
    unsigned char normalized;

    static unsigned int get_size_of_type(unsigned int type)
    {
        switch (type)
        {
            case GL_FLOAT:
                return 4;
            case GL_UNSIGNED_INT:
                return 4;
            case GL_UNSIGNED_BYTE:
                return 1;
        }

        return 0;
    }
};

class vertex_buffer_layout
{
public:
    vertex_buffer_layout()
        : stride_(0)
    {
    }

    void push_float(unsigned int count)
    {
        elements_.push_back({ GL_FLOAT, count, GL_FALSE });
        stride_ += count * vertex_buffer_element::get_size_of_type(GL_FLOAT);
    }

    void push_unsigned_int(unsigned int count)
    {
        elements_.push_back({ GL_UNSIGNED_INT, count, GL_FALSE });
        stride_ += count * vertex_buffer_element::get_size_of_type(GL_UNSIGNED_INT);
    }

    void push_unsigned_char(unsigned int count)
    {
        elements_.push_back({ GL_UNSIGNED_BYTE, count, GL_TRUE });
        stride_ += count * vertex_buffer_element::get_size_of_type(GL_UNSIGNED_BYTE);
    }

    const std::vector<vertex_buffer_element> get_elements() const
    {
        return elements_;
    }

    unsigned int get_stride() const { return stride_; }

private:
    std::vector<vertex_buffer_element> elements_;
    unsigned int                       stride_;
};