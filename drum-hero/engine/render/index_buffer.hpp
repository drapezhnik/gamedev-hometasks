#pragma once

class index_buffer
{
public:
    index_buffer(const unsigned int* data, unsigned int count);
    ~index_buffer();

    void bind() const;
    void unbind() const;

    unsigned int get_count() const;

private:
    unsigned int gl_buffer_id_;
    unsigned int count_;
};