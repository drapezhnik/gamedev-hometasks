#include "texture.hpp"
#include "engine/engine_gl_debug.hpp"
#include "vendor/glad/glad.h"
#include "vendor/stb/stb_image.hpp"

#include <fstream>

texture::texture(const std::string& path)
    : gl_texture_id_(0)
    , file_path_(path)
    , local_buffer_(nullptr)
    , width_(0)
    , height_(0)
    , bpp_(0)
{
    std::ifstream file(path.data(), std::ios::binary);

    stbi_set_flip_vertically_on_load(1);
    local_buffer_ = stbi_load(path.c_str(), &width_, &height_, &bpp_, 3);

    // Generate texture name
    glGenTextures(1, &gl_texture_id_);
    GL_CHECK()

    // Select the texture in OpenGL context by name
    glBindTexture(GL_TEXTURE_2D, gl_texture_id_);
    GL_CHECK()

    // Set params for texture
    glTexParameteri(GL_TEXTURE_2D,
                    GL_TEXTURE_MIN_FILTER,
                    GL_NEAREST); // TODO: Try GL_LINEAR
    GL_CHECK()

    glTexParameteri(GL_TEXTURE_2D,
                    GL_TEXTURE_MAG_FILTER,
                    GL_NEAREST); // TODO: Try GL_LINEAR
    GL_CHECK()

    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGB,
                 width_,
                 height_,
                 0,
                 GL_RGB,
                 GL_UNSIGNED_BYTE,
                 local_buffer_);
    GL_CHECK()

    // unbind texture
    glBindTexture(GL_TEXTURE_2D, 0);
    GL_CHECK()

    if (local_buffer_)
    {
        stbi_image_free(local_buffer_);
    }
}

texture::~texture()
{
    glDeleteTextures(1, &gl_texture_id_);
    GL_CHECK()
}

void texture::bind(unsigned int slot) const
{
    glActiveTexture(GL_TEXTURE0 + slot);
    GL_CHECK()

    glBindTexture(GL_TEXTURE_2D, gl_texture_id_);
    GL_CHECK()
}

void texture::unbind() const
{
    glBindTexture(GL_TEXTURE_2D, 0);
    GL_CHECK()
}
