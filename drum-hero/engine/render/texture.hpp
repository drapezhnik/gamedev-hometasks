#pragma once

#include <string>

class texture
{
public:
    texture(const std::string& path);
    ~texture();

    void bind(unsigned int slot = 0) const;
    void unbind() const;

private:
    unsigned int   gl_texture_id_;
    std::string    file_path_;
    unsigned char* local_buffer_;
    int            width_;
    int            height_;
    int            bpp_;
};
