#include "index_buffer.hpp"
#include "engine/engine_gl_debug.hpp"
#include "vendor/glad/glad.h"

index_buffer::index_buffer(const unsigned int* data, unsigned int count)
    : count_(count)
{
    // As I started using native c++ numeric types for OpenGL object indexes
    // just make sure that platform implementation for OpenGL types match the
    // size of native ones
    assert(sizeof(unsigned int) == sizeof(GLuint));

    glGenBuffers(1, &gl_buffer_id_);
    GL_CHECK()

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl_buffer_id_);
    GL_CHECK()

    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 count * sizeof(unsigned int),
                 data,
                 GL_STATIC_DRAW);
    GL_CHECK()
}

index_buffer::~index_buffer()
{
    glDeleteBuffers(1, &gl_buffer_id_);
}

void index_buffer::bind() const
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl_buffer_id_);
}

void index_buffer::unbind() const
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

unsigned int index_buffer::get_count() const
{
    return count_;
}
