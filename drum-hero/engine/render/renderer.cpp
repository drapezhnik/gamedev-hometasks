#include "renderer.hpp"
#include "engine/engine_gl_debug.hpp"

#include <iostream>

void renderer::draw(const vertex_array& va,
                    const index_buffer& ib,
                    const shader&       shader) const
{
    shader.bind();
    va.bind();
    ib.bind();

    glDrawElements(GL_TRIANGLES, ib.get_count(), GL_UNSIGNED_INT, nullptr);
    GL_CHECK()
}

void renderer::clear() const
{
    glClear(GL_COLOR_BUFFER_BIT);
    GL_CHECK()
}

void renderer::clear(uint8_t r, uint8_t g, uint8_t b) const
{
    auto r_gl_float = static_cast<GLfloat>(r);
    auto g_gl_float = static_cast<GLfloat>(g);
    auto b_gl_float = static_cast<GLfloat>(b);

    GLfloat r_normalized = 1.f / 255.f * r_gl_float;
    GLfloat g_normalized = 1.f / 255.f * g_gl_float;
    GLfloat b_normalized = 1.f / 255.f * b_gl_float;

    glClearColor(r_normalized, g_normalized, b_normalized, 0.0f);
    GL_CHECK()

    clear();
}
