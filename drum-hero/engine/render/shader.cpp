#include "shader.hpp"
#include "engine/engine_gl_debug.hpp"
#include "vendor/glad/glad.h"

#include <fstream>
#include <sstream>

shader::shader(const std::string& path_to_vertex_shader_src,
               const std::string& path_to_fragment_shader_src)
    : gl_shader_id_(0)
{
    std::string vertex_shader_src   = parse_shader(path_to_vertex_shader_src);
    std::string fragment_shader_src = parse_shader(path_to_fragment_shader_src);
    gl_shader_id_ = create_shader(vertex_shader_src, fragment_shader_src);
}

shader::~shader()
{
    glDeleteProgram(gl_shader_id_);
    GL_CHECK()
}

void shader::bind() const
{
    glUseProgram(gl_shader_id_);
    GL_CHECK()
}

void shader::unbind() const
{
    glUseProgram(0);
    GL_CHECK()
}

void shader::set_uniform_1i(const std::string& name, int value)
{
    glUniform1i(get_uniform_location(name), value);
    GL_CHECK()
}

void shader::set_uniform_4f(const std::string& name,
                            float              value1,
                            float              value2,
                            float              value3,
                            float              value4)
{
    glUniform4f(get_uniform_location(name), value1, value2, value3, value4);
    GL_CHECK()
}

std::string shader::parse_shader(const std::string& path_to_src)
{
    std::ifstream      file(path_to_src.data());
    std::ostringstream shader_src_buf;

    shader_src_buf << file.rdbuf();

    return shader_src_buf.str();
}

unsigned int shader::create_shader(const std::string& vertex_shader_src,
                                   const std::string& fragment_shader_src)
{
    unsigned int program_id = glCreateProgram();
    unsigned int vertex_shader =
        compile_shader(GL_VERTEX_SHADER, vertex_shader_src);
    unsigned int fragment_shader =
        compile_shader(GL_FRAGMENT_SHADER, fragment_shader_src);

    glAttachShader(program_id, vertex_shader);
    GL_CHECK()

    glAttachShader(program_id, fragment_shader);
    GL_CHECK()

    glLinkProgram(program_id);
    GL_CHECK()

    glValidateProgram(program_id);
    GL_CHECK()

    glDeleteShader(vertex_shader);
    GL_CHECK()

    glDeleteShader(fragment_shader);
    GL_CHECK()

    return program_id;
}

unsigned int shader::compile_shader(unsigned int       type,
                                    const std::string& source)
{
    unsigned int shader_id = glCreateShader(type);
    const char*  src       = source.c_str();

    glShaderSource(shader_id, 1, &src, nullptr);
    GL_CHECK()

    glCompileShader(shader_id);
    GL_CHECK()

    return shader_id;
}

unsigned int shader::get_uniform_location(const std::string& name)
{
    unsigned int location = glGetUniformLocation(gl_shader_id_, name.c_str());
    GL_CHECK()

    return location;
}
