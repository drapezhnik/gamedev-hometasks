#include "vertex_array.hpp"
#include "engine/engine_gl_debug.hpp"

vertex_array::vertex_array()
{
    glGenVertexArrays(1, &gl_vertex_array_id_);
    GL_CHECK()
}
vertex_array::~vertex_array()
{
    glDeleteVertexArrays(1, &gl_vertex_array_id_);
    GL_CHECK()
}

void vertex_array::add_buffer(const vertex_buffer&        vb,
                              const vertex_buffer_layout& layout)
{
    bind();
    vb.bind();
    const auto&  elements = layout.get_elements();
    unsigned int offset   = 0;

    for (int i = 0; i < elements.size(); i++)
    {
        const auto& element = elements[i];
        glEnableVertexAttribArray(i);
        glVertexAttribPointer(i,
                              element.count,
                              element.type,
                              element.normalized,
                              layout.get_stride(),
                              (const void*)offset);

        GL_CHECK()

        offset += element.count *
                  vertex_buffer_element::get_size_of_type(element.type);
    }
}

void vertex_array::bind() const
{
    glBindVertexArray(gl_vertex_array_id_);
    GL_CHECK()
}

void vertex_array::unbind() const
{
    glBindVertexArray(0);
    GL_CHECK()
}
