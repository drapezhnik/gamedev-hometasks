#include "vertex_buffer.hpp"
#include "engine/engine_gl_debug.hpp"
#include "vendor/glad/glad.h"

vertex_buffer::vertex_buffer(const void* data, unsigned int size)
{
    glGenBuffers(1, &buffer_id_);
    GL_CHECK()

    glBindBuffer(GL_ARRAY_BUFFER, buffer_id_);
    GL_CHECK()

    glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
    GL_CHECK()
}

vertex_buffer::~vertex_buffer()
{
    glDeleteBuffers(1, &buffer_id_);
    GL_CHECK()
}

void vertex_buffer::bind() const
{
    glBindBuffer(GL_ARRAY_BUFFER, buffer_id_);
    GL_CHECK()
}
void vertex_buffer::unbind() const
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    GL_CHECK()
}
