#include "fps.hpp"

fps::fps(int fps)
{
    this->value_ = fps;
    delay_time = 1000.0f / this->value_;
    start();
}
void fps::start()
{
    next_frame_start = SDL_GetTicks() + delay_time;
}

// в жизненном цикле - fps.Wait()
void fps::wait()
{
    int dt_frame_time = next_frame_start - SDL_GetTicks();      //  сколько времени нужно ждать
    if (dt_frame_time > 0) SDL_Delay(dt_frame_time);            //  если время прошло, то не задерживаемся
    next_frame_start = SDL_GetTicks() + delay_time;          //во сколько нужно стартануть следующий фрэйм
}