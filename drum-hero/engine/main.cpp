#include "engine/game_load/hot_loader.hpp"
#include "fps.hpp"
#include "iengine.hpp"
#include "igame.hpp"

#include <cstdlib>
#include <iostream>

int main(int, char**)
{
    std::string game_lib_name = "game";
    hot_loader  hot_loader(game_lib_name);
    iengine*    engine = create_engine();
    igame*      game   = hot_loader.load_game(engine);
    fps fps(60);

    if (!game->init())
    {
        std::cerr << "error: failed to init the game" << std::endl;
        return EXIT_FAILURE;
    }

    std::vector<action> actions;

    while (engine->process_input(&actions))
    {
        //        if (hot_loader.is_game_updated())
        //        {
        //            game = hot_loader.load_game(engine);
        //        }
        fps.wait();
        game->on_action(actions);
        game->simulate();
        game->render();
    }

    game->shut_down();
    hot_loader.unload_game();
    destroy_engine(engine);

    return EXIT_SUCCESS;
}