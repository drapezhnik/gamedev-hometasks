#pragma once

#include "action_type.hpp"

class action
{
public:
    action_type act_type;
    bool        pressed;
};