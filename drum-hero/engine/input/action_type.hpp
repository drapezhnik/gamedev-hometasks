#pragma once

enum class action_type
{
    UNKNOWN,
    QUIT,
    UP,
    LEFT,
    DOWN,
    RIGHT,
    BTN1,
    BTN2,
    BTN3,
    BTN4,
    BTN5,
};