#pragma once

#include "action.hpp"
#include <vector>

class input_processor
{
public:
    bool process_input(std::vector<action>* actions);
};
