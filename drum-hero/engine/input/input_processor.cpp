#include "input_processor.hpp"

#include <SDL.h>

#include <iostream>
#include <map>
#include <string>

bool input_processor::process_input(std::vector<action>* actions)
{
    // map SDL keycodes to engine's actions
    // clang-format off
    const static std::map<SDL_Keycode, action_type> sdl_to_engine_mappings = {
        { SDL_SCANCODE_W, action_type::UP },
        { SDL_SCANCODE_S, action_type::DOWN },
        { SDL_SCANCODE_A, action_type::LEFT },
        { SDL_SCANCODE_D, action_type::RIGHT },
        { SDL_SCANCODE_F, action_type::BTN1 },
        { SDL_SCANCODE_G, action_type::BTN2 },
        { SDL_SCANCODE_H, action_type::BTN3 },
        { SDL_SCANCODE_J, action_type::BTN4 },
        { SDL_SCANCODE_K, action_type::BTN5 },
        { SDL_SCANCODE_ESCAPE, action_type::QUIT }
    };
    // clang-format on

    SDL_PumpEvents();
    SDL_EventState(SDL_MOUSEMOTION, SDL_IGNORE);

    const Uint8* keyboard_state = SDL_GetKeyboardState(nullptr);

    if (keyboard_state[SDL_SCANCODE_ESCAPE])
        return false;

    actions->clear();

    for (auto& mapping : sdl_to_engine_mappings)
    {
        bool is_key_pressed = static_cast<bool>(keyboard_state[mapping.first]);

        if (is_key_pressed)
        {
            action a{};
            a.act_type = mapping.second;
            a.pressed  = is_key_pressed;

            actions->push_back(a);
        }
    }

    return true;
};