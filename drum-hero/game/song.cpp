#include "song.hpp"

#include <algorithm>
#include <fstream>
#include <iostream>

song::song(iengine*           engine,
           const std::string& song_file_name,
           const std::string& notes_file_name)
    : engine_(engine)
    , notes_file_name_(notes_file_name)
{
    audio_buffer_ = engine_->create_sound_buffer(song_file_name);

    // BEGIN DEBUG OUTPUT

    for (int i = 0; i < all_notes_.size(); i++)
    {
        std::cout << "note " << i + 1 << ": " << all_notes_[i].hit_time
                  << std::endl;
    }

    // END DEBUG OUTPUT
}

song::~song() {}

void song::start(bool looped)
{
    float time_from_init =
        static_cast<float>(engine_->get_milisec_count_from_init());

    start_time_ = static_cast<float>(time_from_init);
    audio_buffer_->play(looped);
}

void song::update()
{
    float time_from_init =
        static_cast<float>(engine_->get_milisec_count_from_init());

    song_position_in_sec_   = (time_from_init - start_time_) / 1000;
    song_position_in_beats_ = song_position_in_sec_ / sec_per_beat_;

    //    std::cout << "song_position_in_sec_: " << song_position_in_sec_
    //              << std::endl;
}

std::vector<drum_note> song::get_all_notes()
{
    if (all_notes_.empty())
    {
        // TODO: move to class fields
        float start_point_x = -0.675;
        float start_point_y = 0.95;

        float offset_x = 0.35;
        float offset_y = 0.239285714; // 8 notes on screen

        float element_size_x = 0.050;
        float element_size_y = 0.050;

        std::ifstream file(notes_file_name_);

        std::string line;
        for (int i = 0; std::getline(file, line); i++)
        {
            if (i == 0)
            {
                bpm_ = std::stof(line);
                continue;
            }
            if (i == 1)
            {
                note_size_    = std::stof(line);
                sec_per_beat_ = 60.f / (note_size_ / 4 * bpm_);
                continue;
            }

            for (int j = 0; j < line.length(); j++)
            {
                if (line[j] == '/')
                {
                    continue;
                }

                if (line[j] == 'x')
                {
                    drum_note dp;
                    float     note_line  = static_cast<float>(i - 2);
                    float     note_index = static_cast<float>(j);

                    // clang-format off

                    dp.v1.x = (note_index * offset_x) + start_point_x;
                    dp.v1.y = (note_line * offset_y) + start_point_y + element_size_y;
                    dp.v1.u = 0.0;
                    dp.v1.v = 1.0;

                    dp.v2.x = dp.v1.x + element_size_x;
                    dp.v2.y = dp.v1.y;
                    dp.v2.u = 1.0;
                    dp.v2.v = 1.0;

                    dp.v3.x = dp.v2.x;
                    dp.v3.y = dp.v2.y - element_size_y;
                    dp.v3.u = 1.0;
                    dp.v3.v = 0.0;

                    dp.v4.x = dp.v3.x - element_size_x;
                    dp.v4.y = dp.v3.y;
                    dp.v4.u = 0.0;
                    dp.v4.v = 0.0;

                    // clang-format on

                    dp.path_to_texture = get_note_texture_path(j);
                    dp.type            = get_note_drum_type(j);
                    dp.hit_time =
                        static_cast<float>(note_line + 1) * get_sec_per_beat();

                    all_notes_.push_back(dp);
                }
            }
        }
    }

    return all_notes_;
}

std::vector<drum_note> song::get_specific_drum_notes(drum_type type)
{
    if (all_notes_.empty())
    {
        get_all_notes();
    }

    std::vector<drum_note> result;

    std::for_each(all_notes_.begin(),
                  all_notes_.end(),
                  [&](drum_note& note)
                  {
                      if (note.type == type)
                      {
                          result.push_back(note);
                      }
                  });

    //    std::copy_if(all_notes_.begin(),
    //                  all_notes_.end(),
    //                 result.begin(),
    //                  [&](drum_note& note)
    //                  {
    //                     return note.type == type;
    //                  });

    return result;
}

float song::get_bpm()
{
    return bpm_;
}

float song::get_sec_per_beat()
{
    return sec_per_beat_;
}

float song::get_note_size_()
{
    return note_size_;
}

float song::get_song_position_in_sec()
{
    return song_position_in_sec_;
}

float song::get_song_position_in_beats()
{
    return song_position_in_beats_;
}

float song::get_start_time()
{
    return start_time_;
}

float song::get_start_time_in_sec()
{
    return start_time_ / 1000;
}

std::string song::get_note_texture_path(int position)
{
    std::string path_to_yellow_circle_texture =
        "resources/textures/yellow_circle.png";
    std::string path_to_orange_circle_texture =
        "resources/textures/orange_circle.png";
    std::string path_to_red_circle_texture =
        "resources/textures/red_circle.png";
    std::string path_to_green_circle_texture =
        "resources/textures/green_circle.png";
    std::string path_to_blue_circle_texture =
        "resources/textures/blue_circle.png";

    switch (position)
    {
        case 0:
            return path_to_blue_circle_texture;
        case 1:
            return path_to_red_circle_texture;
        case 2:
            return path_to_yellow_circle_texture;
        case 3:
            return path_to_orange_circle_texture;
        case 4:
            return path_to_green_circle_texture;
            // TODO: Add default and throw error
    }
}

drum_type song::get_note_drum_type(int position)
{
    switch (position)
    {
        case 0:
            return drum_type::BASS;
        case 1:
            return drum_type::SNARE;
        case 2:
            return drum_type::HAT;
        case 3:
            return drum_type::CRASH;
        case 4:
            return drum_type::RIDE;
            // TODO: Add default and throw error
    }
}
