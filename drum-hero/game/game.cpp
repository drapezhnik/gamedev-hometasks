#include "game.hpp"
#include "drum_note.hpp"
#include "engine/iengine.hpp"

#include <algorithm>
#include <engine/math/vector2.hpp>
#include <iostream>

game::game(iengine* engine)
    : engine_(engine)
{
}

bool game::init()
{
    if (!engine_->init("drum-hero",
                       1920,
                       1080,
                       "resources/shaders/vertex.shader",
                       "resources/shaders/fragment.shader"))
    {
        std::cerr << "error: failed to init an engine" << std::endl;
        return false;
    }

    std::string path_to_snare_texture = "resources/textures/snare_drum.png";
    std::string path_to_bass_texture  = "resources/textures/bass_drum.png";
    std::string path_to_hihat_texture = "resources/textures/hihat_cymbal.png";
    std::string path_to_crash_texture = "resources/textures/crash_cymbal.png";
    std::string path_to_ride_texture  = "resources/textures/ride_cymbal.png";

    drum_note snare;
    drum_note bass;
    drum_note hihat;
    drum_note crash;
    drum_note ride;
    drum_note red_line;

    bass.v1              = { -0.80, -0.7, 0.0, 1.0 };
    bass.v2              = { -0.55, -0.7, 1.0, 1.0 };
    bass.v3              = { -0.55, -0.9, 1.0, 0.0 };
    bass.v4              = { -0.80, -0.9, 0.0, 0.0 };
    bass.path_to_texture = path_to_bass_texture;

    snare.v1              = { -0.45, -0.7, 0.0, 1.0 };
    snare.v2              = { -0.20, -0.7, 1.0, 1.0 };
    snare.v3              = { -0.20, -0.9, 1.0, 0.0 };
    snare.v4              = { -0.45, -0.9, 0.0, 0.0 };
    snare.path_to_texture = path_to_snare_texture;

    hihat.v1              = { -0.10, -0.7, 0.0, 1.0 };
    hihat.v2              = { 0.15, -0.7, 1.0, 1.0 };
    hihat.v3              = { 0.15, -0.9, 1.0, 0.0 };
    hihat.v4              = { -0.10, -0.9, 0.0, 0.0 };
    hihat.path_to_texture = path_to_hihat_texture;

    crash.v1              = { 0.25, -0.7, 0.0, 1.0 };
    crash.v2              = { 0.50, -0.7, 1.0, 1.0 };
    crash.v3              = { 0.50, -0.9, 1.0, 0.0 };
    crash.v4              = { 0.25, -0.9, 0.0, 0.0 };
    crash.path_to_texture = path_to_crash_texture;

    ride.v1              = { 0.60, -0.7, 0.0, 1.0 };
    ride.v2              = { 0.85, -0.7, 1.0, 1.0 };
    ride.v3              = { 0.85, -0.9, 1.0, 0.0 };
    ride.v4              = { 0.60, -0.9, 0.0, 0.0 };
    ride.path_to_texture = path_to_ride_texture;

    red_line.v1              = { -1.f, -0.7, 0.0, 1.0 };
    red_line.v2              = { 1.f, -0.7, 1.0, 1.0 };
    red_line.v3              = { 1.f, -0.702, 1.0, 0.0 };
    red_line.v4              = { -1.f, -0.702, 0.0, 0.0 };
    red_line.path_to_texture = "resources/textures/red_line.png";

    static_drums_.push_back(snare);
    static_drums_.push_back(bass);
    static_drums_.push_back(hihat);
    static_drums_.push_back(crash);
    static_drums_.push_back(ride);
    static_drums_.push_back(red_line);

    //    hihat_ = engine_->create_sound_buffer("resources/sounds/hihat.wav");
    //    snare_ = engine_->create_sound_buffer("resources/sounds/snare.wav");
    //    bass_  = engine_->create_sound_buffer("resources/sounds/bass.wav");

    main_theme_ = new song(engine_,
                           "resources/sounds/song1.wav",
                           "resources/levels/highway_to_hell_track.txt");

    bass_track_.track_notes =
        main_theme_->get_specific_drum_notes(drum_type::BASS);
    snare_track_.track_notes =
        main_theme_->get_specific_drum_notes(drum_type::SNARE);
    hihat_track_.track_notes =
        main_theme_->get_specific_drum_notes(drum_type::HAT);
    crash_track_.track_notes =
        main_theme_->get_specific_drum_notes(drum_type::CRASH);
    ride_track_.track_notes =
        main_theme_->get_specific_drum_notes(drum_type::RIDE);

    float distance     = (-0.9) - 1.f;
    float note_size    = main_theme_->get_note_size_();
    float sec_per_beat = main_theme_->get_sec_per_beat();

    speed_ = (distance / note_size) / sec_per_beat;

    main_theme_->start(false);

    return true;
}

void game::shut_down()
{
    engine_->shut_down();
}

void game::on_action(std::vector<action>& actions)
{
    for (auto& a : actions)
    {
        // TODO: move on_action logic here
        if (a.pressed)
        {
            if (to_render_.empty())
            {
                return;
            }

            drum_type d_type;
            switch (a.act_type)
            {
                case action_type::BTN1:
                    d_type = drum_type::BASS;
                    break;
                case action_type::BTN2:
                    d_type = drum_type::SNARE;
                    break;
                case action_type::BTN3:
                    d_type = drum_type::HAT;
                    break;
                case action_type::BTN4:
                    d_type = drum_type::CRASH;
                    break;
                case action_type::BTN5:
                    d_type = drum_type::RIDE;
                    break;
            }

            drum_track* track = get_track_by_drum_type(d_type);
            auto        note  = track->get_note_to_play(); // FIXME: exception


            float key_down_time = main_theme_->get_song_position_in_sec();
            float song_start_time_in_sec = main_theme_->get_start_time_in_sec();
            float note_hit_time   = note.hit_time + song_start_time_in_sec;
            float acceptable_miss = 0.25;

            if (key_down_time < note_hit_time + acceptable_miss &&
                key_down_time > note_hit_time - acceptable_miss)
            {
                std::cout << "success" << std::endl;
            }
            else
            {
                std::cout << "fail" << std::endl;
            }

            std::cout << "key_down_time: " << key_down_time
                      << std::endl;
            std::cout << "note_hit_time: " << note_hit_time
                      << std::endl;
            std::cout
                << "note_to_play_index: " << track->note_to_play_index
                << std::endl;
            std::cout << "note.v1: " << note.v1.y << std::endl;
            std::cout << "note.v2: " << note.v2.y << std::endl;
            std::cout << "note.v3: " << note.v3.y << std::endl;
            std::cout << "note.v4: " << note.v4.y << std::endl;
        }
    }
}

void game::simulate()
{
    // TODO: move simulate logic here
    main_theme_->update();

    float new_time = main_theme_->get_song_position_in_sec();

    if (time_from_last_move_ == -1.f)
    {
        time_from_last_move_ = new_time;
        return;
    }

    float move_delta = speed_ * (new_time - time_from_last_move_);

    if (move_delta == 0.f)
    {
        return;
    }

    time_from_last_move_ = new_time;

    matrix3x3 m;
    m(1, 2) += move_delta;

    to_render_.clear();

    for (auto& track : get_all_tracks())
    {
        for (auto& note : track->track_notes)
        {
            vector2 v1 = { note.v1.x, note.v1.y };
            vector2 v2 = { note.v2.x, note.v2.y };
            vector2 v3 = { note.v3.x, note.v3.y };
            vector2 v4 = { note.v4.x, note.v4.y };

            vector2 v1_new = m * v1;
            vector2 v2_new = m * v2;
            vector2 v3_new = m * v3;
            vector2 v4_new = m * v4;

            note.v1.y = v1_new.y;
            note.v2.y = v2_new.y;
            note.v3.y = v3_new.y;
            note.v4.y = v4_new.y;

            if (note.v4.y >= -0.7f && note.v4.y <= 1.f)
            {
                to_render_.push_back(note);
            }

            if (note.v4.y <= -0.7 && !note.is_played)
            {
                note.is_played = true;
                track->note_to_play_index++;
            }
        }
    }
}

void game::render()
{
    engine_->clear_window(0, 0, 0);
    //    engine_->clear_window(255, 255, 255);

    for (auto& drum : static_drums_)
    {
        std::vector<vertex> vertexes = { drum.v1, drum.v2, drum.v3, drum.v4 };
        std::vector<unsigned int> indexes = { 0, 1, 2, 0, 2, 3 };

        engine_->render(vertexes, indexes, drum.path_to_texture);
    }

    for (auto& obj : to_render_)
    {
        std::vector<vertex>       vertexes = { obj.v1, obj.v2, obj.v3, obj.v4 };
        std::vector<unsigned int> indexes  = { 0, 1, 2, 0, 2, 3 };

        engine_->render(vertexes, indexes, obj.path_to_texture);
    }

    engine_->swap_buffers();
}

std::vector<drum_track*> game::get_all_tracks()
{
    return {
        &bass_track_, &snare_track_, &hihat_track_, &crash_track_, &ride_track_
    };
}

drum_track* game::get_track_by_drum_type(drum_type type)
{
    switch (type)
    {
        case drum_type::BASS:
            return &bass_track_;
            break;
        case drum_type::SNARE:
            return &snare_track_;
            break;
        case drum_type::HAT:
            return &hihat_track_;
            break;
        case drum_type::CRASH:
            return &crash_track_;
            break;
        case drum_type::RIDE:
            return &ride_track_;
            break;
    }
}

igame* create_game(iengine* engine)
{
    auto g = new game(engine);
    return g;
}

void destroy_game(igame* game)
{
    delete game;
}