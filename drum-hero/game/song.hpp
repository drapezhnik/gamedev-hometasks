#pragma once

#include "drum_note.hpp"
#include "engine/audio/iaudio_buffer.hpp"
#include "engine/iengine.hpp"

#include <vector>

class song
{
private:
    iengine*       engine_;
    iaudio_buffer* audio_buffer_;
    std::string    notes_file_name_;

    std::vector<drum_note> all_notes_;
    float                  bpm_;
    float                  note_size_;
    float                  sec_per_beat_;
    float                  start_time_;
    float                  song_position_in_sec_;
    float                  song_position_in_beats_;

public:
    song(iengine*           engine,
         const std::string& song_file_name,
         const std::string& notes_file_name);
    ~song();

    void                   start(bool looped);
    void                   update();
    std::vector<drum_note> get_all_notes();
    std::vector<drum_note> get_specific_drum_notes(drum_type type);
    float                  get_bpm();
    float                  get_sec_per_beat();
    float                  get_note_size_();
    float                  get_song_position_in_sec();
    float                  get_song_position_in_beats();
    float                  get_start_time();
    float                  get_start_time_in_sec();

private:
    std::string get_note_texture_path(int position);
    drum_type   get_note_drum_type(int position);
};
