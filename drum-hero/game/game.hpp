#include "drum_track.hpp"
#include "engine/audio/iaudio_buffer.hpp"
#include "engine/igame.hpp"
#include "engine/math/matrix3x3.hpp"
#include "song.hpp"

#include <iostream>
#include <vector>

class game : public igame
{
private:
    iengine*               engine_ = nullptr;
    std::vector<drum_note> static_drums_;
    std::vector<drum_note> to_render_;

    drum_track bass_track_;
    drum_track snare_track_;
    drum_track hihat_track_;
    drum_track crash_track_;
    drum_track ride_track_;

    float time_from_last_move_ = -1.f;
    float speed_; // per second

    song*          main_theme_;
    iaudio_buffer* snare_;
    iaudio_buffer* bass_;
    iaudio_buffer* hihat_;

public:
    game(iengine* engine);

    bool init() override;
    void shut_down() override;

    void on_action(std::vector<action>& actions) override;
    void simulate() override;
    void render() override;

private:
    std::vector<drum_track*> get_all_tracks();
    drum_track* get_track_by_drum_type(drum_type type);
};
