#pragma once

#include "drum_note.hpp"
#include <vector>

class drum_track
{
public:
    drum_track();
    ~drum_track();

    std::vector<drum_note> track_notes;
    int                    note_to_play_index = 0;

    drum_note& get_note_to_play();
};
