#pragma once

#include "iengine.hpp"
#include "vendor/glad/glad.h"

#include <SDL.h>
#include <string>
#include <string_view>

class engine : public iengine
{
public:
    bool init(int              window_width,
              int              window_height,
              std::string_view path_to_texture,
              std::string_view path_to_vertex_shader_src,
              std::string_view path_to_fragment_shader_src) override;
    void shut_down() override;

    bool process_input(action* action) override;
    void swap_buffers() override;
    void clear_window(uint8_t r, uint8_t g, uint8_t b) override;
    void render_triangle(vertex& v1, vertex& v2, vertex& v3) override;

private:
    SDL_Window* window_        = nullptr;
    GLuint      gl_program_id_ = 0;

    void   init_gfx_program(std::string_view path_to_texture,
                            std::string_view path_to_vertex_shader_src,
                            std::string_view path_to_fragment_shader_src);
    bool   init_texture(std::string_view path);
    GLuint init_shader(GLenum shader_type, std::string_view shader_src_path);
    std::string load_shader_src(std::string_view path);
};