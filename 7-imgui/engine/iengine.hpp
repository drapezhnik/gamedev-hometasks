#pragma once

#include <cassert>
#include <cstdint>
#include <string_view>
#include <vector>

enum class action_type
{
    UNKNOWN,
    QUIT,
    UP,
    LEFT,
    BACK,
    RIGHT,
    BTN1,
    BTN2,
    BTN3,
    BTN4
};

class action
{
public:
    action_type act_type;
    bool        pressed;
};

class vertex
{
public:
    float x = 0.f;
    float y = 0.f;
    float u = 0.f; // texture coordinate x
    float v = 0.f; // texture coordinate y
};

class matrix3x3
{
public:
    matrix3x3() { data_.resize(side_size * side_size); }

    float& operator()(int row, int column)
    {
        assert(row >= 0 && row < side_size);
        assert(column >= 0 && column < side_size);

        int index = row * side_size + column;
        return data_[index];
    }

    matrix3x3 operator*(matrix3x3 r_matrix)
    {
        matrix3x3 result;

        result(0, 0) = data_.at(0 * side_size + 0) * r_matrix(0, 0) +
                       data_.at(0 * side_size + 1) * r_matrix(1, 0) +
                       data_.at(0 * side_size + 2) * r_matrix(2, 0);

        result(0, 1) = data_.at(0 * side_size + 0) * r_matrix(0, 1) +
                       data_.at(0 * side_size + 1) * r_matrix(1, 1) +
                       data_.at(0 * side_size + 2) * r_matrix(2, 1);

        result(0, 2) = data_.at(0 * side_size + 0) * r_matrix(0, 2) +
                       data_.at(0 * side_size + 1) * r_matrix(1, 2) +
                       data_.at(0 * side_size + 2) * r_matrix(2, 2);

        result(1, 0) = data_.at(1 * side_size + 0) * r_matrix(0, 0) +
                       data_.at(1 * side_size + 1) * r_matrix(1, 0) +
                       data_.at(1 * side_size + 2) * r_matrix(2, 0);

        result(1, 1) = data_.at(1 * side_size + 0) * r_matrix(0, 1) +
                       data_.at(1 * side_size + 1) * r_matrix(1, 1) +
                       data_.at(1 * side_size + 2) * r_matrix(2, 1);

        result(1, 2) = data_.at(1 * side_size + 0) * r_matrix(0, 2) +
                       data_.at(1 * side_size + 1) * r_matrix(1, 2) +
                       data_.at(1 * side_size + 2) * r_matrix(2, 2);

        result(2, 0) = data_.at(2 * side_size + 0) * r_matrix(0, 0) +
                       data_.at(2 * side_size + 1) * r_matrix(1, 0) +
                       data_.at(2 * side_size + 2) * r_matrix(2, 0);

        result(2, 1) = data_.at(2 * side_size + 0) * r_matrix(0, 1) +
                       data_.at(2 * side_size + 1) * r_matrix(1, 1) +
                       data_.at(2 * side_size + 2) * r_matrix(2, 1);

        result(2, 2) = data_.at(2 * side_size + 0) * r_matrix(0, 2) +
                       data_.at(2 * side_size + 1) * r_matrix(1, 2) +
                       data_.at(2 * side_size + 2) * r_matrix(2, 2);

        return result;
    }

private:
    int                side_size = 3;
    std::vector<float> data_;
};

class iengine
{
public:
    virtual ~iengine() = default;

    virtual bool init(int              window_width,
                      int              window_height,
                      std::string_view path_to_texture,
                      std::string_view path_to_vertex_shader_src,
                      std::string_view path_to_fragment_shader_src) = 0;
    virtual void shut_down()                                        = 0;

    virtual bool process_input(action* action)                       = 0;
    virtual void swap_buffers()                                      = 0;
    virtual void clear_window(uint8_t r, uint8_t g, uint8_t b)       = 0;
    virtual void render_triangle(vertex& v1, vertex& v2, vertex& v3) = 0;
};

class igame
{
public:
    virtual ~igame() = default;

    virtual bool init()      = 0;
    virtual void shut_down() = 0;

    virtual void on_action(action& action) = 0;
    virtual void simulate()                = 0;
    virtual void render()                  = 0;
};

iengine* create_engine();

void destroy_engine(iengine* engine);

extern "C"
{
    igame* create_game(iengine* engine);
    void   destroy_game(igame* game);
}