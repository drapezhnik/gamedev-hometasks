#include <cmath>
#include <iostream>

#include "engine/iengine.hpp"
#include "game.hpp"

game::game(iengine* engine)
    : engine_(engine)
{
}

bool game::init()
{
    if (!engine_->init(800,
                       600,
                       "resources/textures/red_dragon.png",
                       "resources/shaders/vertex.shader",
                       "resources/shaders/fragment.shader"))
    {
        std::cerr << "error: failed to init an engine" << std::endl;
        return false;
    }

    vertex v1 = { -0.3, 0.3, 0.0, 1.0 };
    vertex v2 = { 0.3, 0.3, 1.0, 1.0 };
    vertex v3 = { 0.3, -0.3, 1.0, 0.0 };
    vertex v4 = { -0.3, -0.3, 0.0, 0.0 };

    geometry_.push_back(v1);
    geometry_.push_back(v2);
    geometry_.push_back(v3);
    geometry_.push_back(v4);

    return true;
}

void game::shut_down()
{
    engine_->shut_down();
}

void game::on_action(action& action)
{
    // TODO: Calculate matrixes

    const float pi = 3.1415926f;

    float step  = 0.025f;
    float angle = 0.f;

    translation_matrix_[0][2] = 0.f;
    translation_matrix_[1][2] = 0.f;

    rotation_matrix_[0][0] = 1.f;
    rotation_matrix_[0][1] = 0.f;
    rotation_matrix_[1][0] = 0.f;
    rotation_matrix_[1][1] = 1.f;

    if (action.pressed)
    {
        switch (action.act_type)
        {
            case action_type::UP:
                translation_matrix_[1][2] += step;
                angle = -pi / 2.f;
                break;

            case action_type::BACK:
                translation_matrix_[1][2] -= step;
                angle = pi / 2.f;
                break;

            case action_type::LEFT:
                translation_matrix_[0][2] -= step;
                angle = 0.f;
                break;

            case action_type::RIGHT:
                translation_matrix_[0][2] += step;
                angle = -pi;
                break;
        }

        rotation_matrix_[0][0] = std::cos(angle);
        rotation_matrix_[0][1] = -std::sin(angle);
        rotation_matrix_[1][0] = std::sin(angle);
        rotation_matrix_[1][1] = std::cos(angle);
    }
}

void game::simulate()
{
    // TODO: Multiply to matrixes

    for (auto& vertex : geometry_)
    {
        float mvp_matrix[3][3];

        mvp_matrix[0][0] = rotation_matrix_[0][0] * translation_matrix_[0][0] +
                           rotation_matrix_[0][1] * translation_matrix_[1][0] +
                           rotation_matrix_[0][2] * translation_matrix_[2][0];

        mvp_matrix[0][1] = rotation_matrix_[0][0] * translation_matrix_[0][1] +
                           rotation_matrix_[0][1] * translation_matrix_[1][1] +
                           rotation_matrix_[0][2] * translation_matrix_[2][1];

        mvp_matrix[0][2] = rotation_matrix_[0][0] * translation_matrix_[0][2] +
                           rotation_matrix_[0][1] * translation_matrix_[1][2] +
                           rotation_matrix_[0][2] * translation_matrix_[2][2];

        mvp_matrix[1][0] = rotation_matrix_[1][0] * translation_matrix_[0][0] +
                           rotation_matrix_[1][1] * translation_matrix_[1][0] +
                           rotation_matrix_[1][2] * translation_matrix_[2][0];

        mvp_matrix[1][1] = rotation_matrix_[1][0] * translation_matrix_[0][1] +
                           rotation_matrix_[1][1] * translation_matrix_[1][1] +
                           rotation_matrix_[1][2] * translation_matrix_[2][1];

        mvp_matrix[1][2] = rotation_matrix_[1][0] * translation_matrix_[0][2] +
                           rotation_matrix_[1][1] * translation_matrix_[1][2] +
                           rotation_matrix_[1][2] * translation_matrix_[2][2];

        mvp_matrix[2][0] = rotation_matrix_[2][0] * translation_matrix_[0][0] +
                           rotation_matrix_[2][1] * translation_matrix_[1][0] +
                           rotation_matrix_[2][2] * translation_matrix_[2][0];

        mvp_matrix[2][1] = rotation_matrix_[2][0] * translation_matrix_[0][1] +
                           rotation_matrix_[2][1] * translation_matrix_[1][1] +
                           rotation_matrix_[2][2] * translation_matrix_[2][1];

        mvp_matrix[2][2] = rotation_matrix_[2][0] * translation_matrix_[0][2] +
                           rotation_matrix_[2][1] * translation_matrix_[1][2] +
                           rotation_matrix_[2][2] * translation_matrix_[2][2];

        float new_x = mvp_matrix[0][0] * vertex.x +
                      mvp_matrix[0][1] * vertex.y + mvp_matrix[0][2];
        float new_y = mvp_matrix[1][0] * vertex.x +
                      mvp_matrix[1][1] * vertex.y + mvp_matrix[1][2];

        vertex.x = new_x;
        vertex.y = new_y;
    }
}

void game::render()
{
    vertex v1 = geometry_[0];
    vertex v2 = geometry_[1];
    vertex v3 = geometry_[2];
    vertex v4 = geometry_[3];

    // BEGIN DEBUG OUTPUT

    //    std::cout << "v1.x = " << v1.x << "v1.y = " << v1.y << std::endl;
    //    std::cout << "v2.x = " << v2.x << "v2.y = " << v2.y << std::endl;
    //    std::cout << "v3.x = " << v3.x << "v3.y = " << v3.y << std::endl;
    //    std::cout << "v4.x = " << v4.x << "v4.y = " << v4.y << std::endl;

    // END DEBUG OUTPUT

    engine_->swap_buffers();
    engine_->clear_window(0, 0, 0);
    engine_->render_triangle(v1, v3, v4);
    engine_->render_triangle(v1, v2, v3);
}

igame* create_game(iengine* engine)
{
    auto g = new game(engine);
    return g;
}

void destroy_game(igame* game)
{
    delete game;
}