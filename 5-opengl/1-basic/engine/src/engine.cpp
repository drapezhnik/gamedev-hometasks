#include "../include/iengine.hpp"

#include <cassert>
#include <iostream>
#include <map>

#include <SDL.h>
#include <chrono>

#include "glad/glad.h"

#define GL_CHECK()                                                             \
    {                                                                          \
        const int err = static_cast<int>(glGetError());                        \
        if (err != GL_NO_ERROR)                                                \
        {                                                                      \
            switch (err)                                                       \
            {                                                                  \
                case GL_INVALID_ENUM:                                          \
                    std::cerr << "GL_INVALID_ENUM" << std::endl;               \
                    break;                                                     \
                case GL_INVALID_VALUE:                                         \
                    std::cerr << "GL_INVALID_VALUE" << std::endl;              \
                    break;                                                     \
                case GL_INVALID_OPERATION:                                     \
                    std::cerr << "GL_INVALID_OPERATION" << std::endl;          \
                    break;                                                     \
                case GL_INVALID_FRAMEBUFFER_OPERATION:                         \
                    std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION"            \
                              << std::endl;                                    \
                    break;                                                     \
                case GL_OUT_OF_MEMORY:                                         \
                    std::cerr << "GL_OUT_OF_MEMORY" << std::endl;              \
                    break;                                                     \
            }                                                                  \
            assert(false);                                                     \
        }                                                                      \
    }

static void APIENTRY
callback_opengl_debug(GLenum                       source,
                      GLenum                       type,
                      GLuint                       id,
                      GLenum                       severity,
                      GLsizei                      length,
                      const GLchar*                message,
                      [[maybe_unused]] const void* userParam);

static const char* source_to_strv(GLenum source);
static const char* type_to_strv(GLenum type);
static const char* severity_to_strv(GLenum severity);

class engine : public iengine
{
public:
    bool init() override
    {
        using namespace std;
        const int sdl_init_result = SDL_Init(SDL_INIT_EVERYTHING);

        if (sdl_init_result != 0)
        {
            cerr << "error: SDL_Init failed: " << SDL_GetError() << endl;
            return false;
        }

        // set flag to enable debug gl context BEFORE sdl window init
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

        window_ = SDL_CreateWindow("5-1-opengl-basic",
                                   SDL_WINDOWPOS_CENTERED,
                                   SDL_WINDOWPOS_CENTERED,
                                   800,
                                   600,
                                   SDL_WINDOW_OPENGL);

        if (window_ == nullptr)
        {
            cerr << "error: SDL_CreateWindow failed: " << SDL_GetError()
                 << endl;
            return false;
        }

        int gl_major_version   = 3;
        int gl_minor_version   = 2;
        int gl_context_profile = SDL_GL_CONTEXT_PROFILE_ES;

        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_version);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_version);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, gl_context_profile);

        SDL_GLContext gl_context = SDL_GL_CreateContext(window_);

        if (gl_context == nullptr)
        {
            cerr << "error: SDL_GL_CreateContext failed: " << SDL_GetError()
                 << endl;
            return false;
        }

        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_version);
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_version);

        clog << "OpenGL context created with version: " << gl_major_version
             << '.' << gl_minor_version << endl;

        if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0)
        {
            clog << "error: failed to initialize glad" << std::endl;
            return false;
        }

        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(callback_opengl_debug, nullptr);
        glDebugMessageControl(
            GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);

        return true;
    }

    void shut_down() override
    {
        SDL_DestroyWindow(window_);
        SDL_Quit();
    }

    bool process_input() override
    {
        //        // map SDL keycodes to engine's actions
        //        const static std::map<SDL_Keycode, ActionName>
        //            sdl_to_engine_mappings = {
        //            { SDLK_w, ActionName::UP },       { SDLK_a,
        //            ActionName::LEFT }, { SDLK_s, ActionName::BACK },     {
        //            SDLK_d, ActionName::RIGHT }, { SDLK_i, ActionName::BTN1 },
        //            { SDLK_j, ActionName::BTN2 }, { SDLK_k, ActionName::BTN3
        //            },     { SDLK_l, ActionName::BTN4 }, { SDLK_ESCAPE,
        //            ActionName::QUIT }
        //        };
        //
        //        // get SDL event
        //        static SDL_Event event; // TODO: test static
        //        SDL_PollEvent(&event);
        //
        //        // quit on dedicated SDL event
        //        if (event.type == SDL_QUIT)
        //        {
        //            return false;
        //        }
        //
        //        // we are not interested in other types of events for now
        //        if(event.type != SDL_KEYDOWN && event.type != SDL_KEYUP)
        //        {
        //            return true;
        //        }
        //
        //        // look for engine's action name by SDL keycode
        //        auto mapping =
        //        sdl_to_engine_mappings.find(event.key.keysym.sym);
        //
        //        // return if not found, but program continues running
        //        if (mapping == std::end(sdl_to_engine_mappings))
        //        {
        //            action.action_name = ActionName::UNKNOWN;
        //            return true;
        //        }
        //
        //        // quit if dedicated key is pressed
        //        if (action.action_name == ActionName::QUIT)
        //        {
        //            return false;
        //        }
        //
        //        // store engine's action name for future output
        //        action.action_name = mapping->second;
        //
        //        // determine key state
        //        switch (event.type)
        //        {
        //            case SDL_KEYDOWN:
        //                action.pressed = true;
        //                break;
        //            case SDL_KEYUP:
        //                action.pressed = false;
        //                break;
        //        }

        return true;
    };

    void clear_window(uint8_t r, uint8_t g, uint8_t b) override
    {
        auto r_gl_float = static_cast<GLfloat>(r);
        auto g_gl_float = static_cast<GLfloat>(g);
        auto b_gl_float = static_cast<GLfloat>(b);

        GLfloat normalized_r = 1.f / 255.f * r_gl_float;
        GLfloat normalized_g = 1.f / 255.f * g_gl_float;
        GLfloat normalized_b = 1.f / 255.f * b_gl_float;

        glClearColor(normalized_r, normalized_g, normalized_b, 0.0f);
        GL_CHECK()
        glClear(GL_COLOR_BUFFER_BIT);
        GL_CHECK()
    };

    void swap_buffers() override { SDL_GL_SwapWindow(window_); }

private:
    SDL_Window* window_ = nullptr;
};

// 30Kb on my system, too much for stack
static std::array<char, GL_MAX_DEBUG_MESSAGE_LENGTH> local_log_buff;

static void APIENTRY
callback_opengl_debug(GLenum                       source,
                      GLenum                       type,
                      GLuint                       id,
                      GLenum                       severity,
                      GLsizei                      length,
                      const GLchar*                message,
                      [[maybe_unused]] const void* userParam)
{
    // The memory for message is owned and managed by the GL, and should
    // only be considered valid for the duration of the function call.The
    // behavior of calling any GL or window system function from within
    // the callback function is undefined and may lead to program
    // termination.Care must also be taken in securing debug callbacks for
    // use with asynchronous debug output by multi-threaded GL
    // implementations.  Section 18.8 describes this in further detail.

    auto& buff{ local_log_buff };
    int   num_chars = std::snprintf(buff.data(),
                                  buff.size(),
                                  "%s %s %d %s %.*s\n",
                                  source_to_strv(source),
                                  type_to_strv(type),
                                  id,
                                  severity_to_strv(severity),
                                  length,
                                  message);

    if (num_chars > 0)
    {
        // TODO use https://en.cppreference.com/w/cpp/io/basic_osyncstream
        // to fix possible data races
        // now we use GL_DEBUG_OUTPUT_SYNCHRONOUS to garantie call in main
        // thread
        std::cerr.write(buff.data(), num_chars);
    }
}

static const char* source_to_strv(GLenum source)
{
    switch (source)
    {
        case GL_DEBUG_SOURCE_API:
            return "API";
        case GL_DEBUG_SOURCE_SHADER_COMPILER:
            return "SHADER_COMPILER";
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
            return "WINDOW_SYSTEM";
        case GL_DEBUG_SOURCE_THIRD_PARTY:
            return "THIRD_PARTY";
        case GL_DEBUG_SOURCE_APPLICATION:
            return "APPLICATION";
        case GL_DEBUG_SOURCE_OTHER:
            return "OTHER";
    }
    return "unknown";
}

static const char* type_to_strv(GLenum type)
{
    switch (type)
    {
        case GL_DEBUG_TYPE_ERROR:
            return "ERROR";
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            return "DEPRECATED_BEHAVIOR";
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            return "UNDEFINED_BEHAVIOR";
        case GL_DEBUG_TYPE_PERFORMANCE:
            return "PERFORMANCE";
        case GL_DEBUG_TYPE_PORTABILITY:
            return "PORTABILITY";
        case GL_DEBUG_TYPE_MARKER:
            return "MARKER";
        case GL_DEBUG_TYPE_PUSH_GROUP:
            return "PUSH_GROUP";
        case GL_DEBUG_TYPE_POP_GROUP:
            return "POP_GROUP";
        case GL_DEBUG_TYPE_OTHER:
            return "OTHER";
    }
    return "unknown";
}

static const char* severity_to_strv(GLenum severity)
{
    switch (severity)
    {
        case GL_DEBUG_SEVERITY_HIGH:
            return "HIGH";
        case GL_DEBUG_SEVERITY_MEDIUM:
            return "MEDIUM";
        case GL_DEBUG_SEVERITY_LOW:
            return "LOW";
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            return "NOTIFICATION";
    }
    return "unknown";
}

iengine* create_engine()
{
    iengine* e = new engine();
    return e;
};

void destroy_engine(iengine* engine)
{
    delete engine;
};