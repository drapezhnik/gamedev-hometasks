#pragma once

#include <cstdint>

class iengine
{
public:
    virtual ~iengine() = default;

    virtual bool init()          = 0;
    virtual void shut_down()     = 0;
    virtual bool process_input() = 0;

    virtual void clear_window(uint8_t r, uint8_t g, uint8_t b) = 0;
    virtual void swap_buffers()                                = 0;
};

class igame
{
public:
    virtual ~igame() = default;

    virtual void on_action() = 0;
    virtual void simulate()  = 0;
    virtual void render()    = 0;
};

iengine* create_engine();
void     destroy_engine(iengine* engine);

extern "C"
{
    igame* create_game(iengine* engine);
    void   destroy_game(igame* game);
}