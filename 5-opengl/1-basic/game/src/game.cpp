#include <iostream>

#include "../../engine/include/iengine.hpp"
#include "../include/game.hpp"

game::game(iengine* engine)
    : engine_(engine)
{
}

void game::on_action() {}

void game::simulate() {}

void game::render()
{
    engine_->clear_window(255,255,255);
    engine_->swap_buffers();
}

igame* create_game(iengine* engine)
{
    auto g = new game(engine);
    return g;
}

void destroy_game(igame* game)
{
    delete game;
}