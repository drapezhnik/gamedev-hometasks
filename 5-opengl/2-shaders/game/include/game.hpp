#include "../../engine/include/iengine.hpp"

#include <iostream>

class game : public igame
{
public:
    game(iengine* engine);

    void on_action() override;
    void simulate() override;
    void render() override;

private:
    iengine* engine_ = nullptr;
};
