cmake_minimum_required(VERSION 3.17)

project(5-2-opengl-shaders CXX C)

set(CMAKE_CXX_STANDARD 17)

add_executable(engine
        engine/include/iengine.hpp
        engine/include/engine.hpp
        engine/include/engine_gl_debug.hpp
        engine/include/hot_loader.hpp
        engine/src/hot_loader.cpp
        engine/src/engine.cpp
        engine/main.cpp

        glad/glad.h
        KHR/khrplatform.h
        glad/glad.c
        )

target_include_directories(engine PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})

set_target_properties(engine PROPERTIES
        ENABLE_EXPORTS 1)

add_library(game SHARED
        game/include/game.hpp
        game/src/game.cpp)

find_package(SDL2 REQUIRED)

target_link_libraries(engine PRIVATE
        SDL2::SDL2)
target_link_libraries(game PRIVATE
        engine)