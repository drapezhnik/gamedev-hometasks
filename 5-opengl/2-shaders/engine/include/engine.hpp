#pragma once

#include "glad/glad.h"
#include "iengine.hpp"

#include <SDL.h>
#include <string_view>

class engine : public iengine
{
public:
    bool init() override;
    void shut_down() override;

    bool process_input(action& action) override;
    void swap_buffers() override;
    void clear_window(uint8_t r, uint8_t g, uint8_t b) override;
    void render_triangle(vertex& v1, vertex& v2, vertex& v3) override;

private:
    SDL_Window* window_        = nullptr;
    GLuint      gl_program_id_ = 0;

    GLuint init_shader(GLenum shader_type, std::string_view shader_src);
    void   init_gl_program();
};