#pragma once

#include <cstdint>

enum class action_type
{
    UNKNOWN,
    QUIT,
    UP,
    LEFT,
    BACK,
    RIGHT,
    BTN1,
    BTN2,
    BTN3,
    BTN4
};

class action
{
public:
    action_type act_type;
    bool        pressed;
};

class vertex
{
public:
    float x = 0.f;
    float y = 0.f;
    float z = 0.f;
};

class iengine
{
public:
    virtual ~iengine() = default;

    virtual bool init()      = 0;
    virtual void shut_down() = 0;

    virtual bool process_input(action& action)                       = 0;
    virtual void swap_buffers()                                      = 0;
    virtual void clear_window(uint8_t r, uint8_t g, uint8_t b)       = 0;
    virtual void render_triangle(vertex& v1, vertex& v2, vertex& v3) = 0;
};

class igame
{
public:
    virtual ~igame() = default;

    virtual void on_action() = 0;
    virtual void simulate()  = 0;
    virtual void render()    = 0;
};

iengine* create_engine();

void destroy_engine(iengine* engine);

extern "C"
{
    igame* create_game(iengine* engine);
    void   destroy_game(igame* game);
}