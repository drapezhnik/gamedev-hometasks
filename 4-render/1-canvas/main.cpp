#include "canvas.hpp"

int main(int, char**)
{
    canvas canvas(6, 6);
    pixel whitePixel{ color{ 255, 255, 255 }, position{ 1, 1 } };
    std::fill(canvas.begin(), canvas.end(), whitePixel);

    canvas.save_image();
}