#pragma once

#include <cstdint>
#include <vector>

#pragma pack(push, 1)
class color
{
public:
    uint8_t r = 0;
    uint8_t g = 0;
    uint8_t b = 0;
};
#pragma pack(pop)

class position
{
public:
    int32_t x = 0;
    int32_t y = 0;
};

class pixel
{
public:
    color    color;
    position position;
};

class canvas
{
public:
    canvas(size_t width, size_t height);

    void  save_image();
    void  load_image();
    pixel get_pixel(size_t x, size_t y);
    void  set_pixel(pixel);
    auto  begin() { return pixels.begin(); }
    auto end() { return pixels.end(); }

private:
    size_t             width;
    size_t             height;
    std::vector<pixel> pixels;
};