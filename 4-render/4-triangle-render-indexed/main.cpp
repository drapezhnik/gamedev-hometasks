#include "include/canvas.hpp"
#include "include/image_manager.hpp"
#include "include/render.hpp"

int main(int, char**)
{
    int           width  = 100;
    int           height = 100;
    canvas        canvas(width, height);
    color         color = { 255, 0, 0 };
    render        render(canvas, color);
    image_manager image_manager;

    render.clear_white();

    std::vector<position> vertex_buffer;

    size_t max_x = 10;
    size_t max_y = 10;

    int32_t step_x = (width - 1) / max_x;
    int32_t step_y = (height - 1) / max_y;

    for (size_t i = 0; i <= max_x; ++i)
    {
        for (size_t j = 0; j <= max_y; ++j)
        {
            position v{ static_cast<int>(i) * step_x,
                        static_cast<int>(j) * step_y };

            vertex_buffer.push_back(v);
        }
    }

    std::vector<uint8_t> index_buffer;

    for (size_t x = 0; x < max_x; ++x)
    {
        for (size_t y = 0; y < max_y; ++y)
        {
            uint8_t index0 = static_cast<uint8_t>(y * (max_y + 1) + x);
            uint8_t index1 = static_cast<uint8_t>(index0 + (max_y + 1) + 1);
            uint8_t index2 = index1 - 1;
            uint8_t index3 = index0 + 1;

            index_buffer.push_back(index0);
            index_buffer.push_back(index1);
            index_buffer.push_back(index2);

            index_buffer.push_back(index0);
            index_buffer.push_back(index3);
            index_buffer.push_back(index1);
        }
    }

    render.render_triangles(vertex_buffer, index_buffer);
    image_manager.save_image(canvas);

    return EXIT_SUCCESS;
}