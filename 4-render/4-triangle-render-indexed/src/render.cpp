#include <algorithm>

#include "../include/image_manager.hpp" // TODO: remove
#include "../include/render.hpp"

render::render(canvas& inner_canvas, color color_to_render)
    : inner_canvas(inner_canvas)
    , color_to_render(color_to_render)
{
}

void render::render_line(position start, position end)
{
    image_manager im;
    auto pixels_to_render = inner_canvas.get_line_pixel_positions(start, end);

    std::for_each(
        pixels_to_render.begin(), pixels_to_render.end(), [&](auto& position) {
            inner_canvas.set_pixel(position, color_to_render);
            // im.save_image(canvas); // Uncomment to generate an image per
            // rendered pixel
        });
}

void render::render_triangle(position v1, position v2, position v3)
{
    render_line(v1, v2);
    render_line(v2, v3);
    render_line(v3, v1);
}

void render::render_triangles(std::vector<position> vertexes, std::vector<uint8_t> indexes)
{
    for (size_t i = 0; i < indexes.size() / 3; i++)
    {
        uint8_t i1 = indexes.at(i * 3);
        uint8_t i2 = indexes.at(i * 3 + 1);
        uint8_t i3 = indexes.at(i * 3 + 2);

        position v1 = vertexes.at(i1);
        position v2 = vertexes.at(i2);
        position v3 = vertexes.at(i3);

        render_triangle(v1, v2, v3);
    }
}

void render::clear_white()
{
    color white{ color{ 255, 255, 255 } };
    std::fill(inner_canvas.begin(), inner_canvas.end(), white);
}

void render::clear_black()
{
    color black{ color{ 0, 0, 0 } };
    std::fill(inner_canvas.begin(), inner_canvas.end(), black);
}