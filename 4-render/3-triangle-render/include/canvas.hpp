#pragma once

#include <cstdint>
#include <vector>

#pragma pack(push, 1)
class color
{
public:
    uint8_t r = 0;
    uint8_t g = 0;
    uint8_t b = 0;
};
#pragma pack(pop)

class position
{
public:
    int x = 0;
    int y = 0;
};

class canvas : public std::vector<color>
{
public:
    canvas(int width, int height);

    int width;
    int height;

    color                 get_pixel_color(position position);
    void                  set_pixel(position position, color color_to_set);
    std::vector<position> get_line_pixel_positions(position start,
                                                   position end);
};