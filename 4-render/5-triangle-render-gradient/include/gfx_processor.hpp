#pragma once

#include <cstddef>
#include <vector>

#include "color.hpp"
#include "uniforms.hpp"
#include "vertex.hpp"

class gfx_processor
{
public:
    vertex vertex_shader(vertex vertex);
    color fragment_shader(vertex vertex);
    void set_uniforms(uniforms u);
    void set_texture(std::vector<color> t);

private:
    uniforms inner_uniforms;
    std::vector<color> texture;
};
