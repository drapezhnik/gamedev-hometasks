#pragma once

class iengine
{
public:
    virtual ~iengine();

    virtual void shut_down()     = 0;
    virtual bool process_input() = 0;
};

class igame
{
public:
    virtual ~igame() = default;

    virtual void on_action() = 0;
    virtual void simulate()  = 0;
    virtual void on_output() = 0;
};

iengine* create_engine();
void     destroy_engine(iengine* engine);

extern "C"
{
    igame* create_game();
    void   destroy_game();
}