#include <iostream>

#include "../include/gfx_game.hpp"

// TODO: Move to better place

size_t width  = 800;
size_t height = 600;

canvas        canvas(width, height);
gfx_processor gfx_processor;
color         color{ 255, 0, 0 };

// TODO

gfx_game::gfx_game(render& render)
    : render_(render)
{
    SDL_Init(SDL_INIT_EVERYTHING);

    window_ = SDL_CreateWindow("GFX game",
                               SDL_WINDOWPOS_CENTERED,
                               SDL_WINDOWPOS_CENTERED,
                               width,
                               height,
                               0);

    if (window_ == nullptr)
    {
        std::cerr << SDL_GetError() << std::endl;
    }

    renderer_ = SDL_CreateRenderer(window_, -1, SDL_RENDERER_ACCELERATED);

    if (renderer_ == nullptr)
    {
        std::cerr << SDL_GetError() << std::endl;
    }
}

gfx_game::~gfx_game()
{
    SDL_DestroyWindow(window_);
    SDL_DestroyRenderer(renderer_);
}

void gfx_game::on_action()
{
    std::cout << "on_action invoked" << std::endl;

    SDL_Event event;
    SDL_PollEvent(&event);
}

void gfx_game::simulate()
{
    std::cout << "simulate invoked" << std::endl;
}

void gfx_game::on_output()
{
    std::cout << "on_output invoked" << std::endl;

    render_.clear_white();

    void*     pixels = canvas.data();
    const int depth  = sizeof(color) * 8;
    const int pitch  = width * sizeof(color);
    const int rmask  = 0x000000ff;
    const int gmask  = 0x0000ff00;
    const int bmask  = 0x00ff0000;
    const int amask  = 0;

    SDL_Surface* bitmapSurface = SDL_CreateRGBSurfaceFrom(
        pixels, width, height, depth, pitch, rmask, gmask, bmask, amask);
    if (bitmapSurface == nullptr)
    {
        std::cerr << SDL_GetError() << std::endl;
    }
    SDL_Texture* bitmapTex =
        SDL_CreateTextureFromSurface(renderer_, bitmapSurface);
    if (bitmapTex == nullptr)
    {
        std::cerr << SDL_GetError() << std::endl;
    }

    SDL_FreeSurface(bitmapSurface);

    SDL_RenderClear(renderer_);
    SDL_RenderCopy(renderer_, bitmapTex, nullptr, nullptr);
    SDL_RenderPresent(renderer_);

    SDL_DestroyTexture(bitmapTex);
}

igame* create_game()
{
    render render(canvas, gfx_processor, color);

    auto game = new gfx_game(render);
    return game;
}

void destroy_game(igame* game)
{
    delete game;
}