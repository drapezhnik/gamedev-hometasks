#include <iostream>

#include "../../engine/include/iengine.hpp"
#include "SDL.h"
#include "render.hpp"

class gfx_game : public igame
{
public:
    gfx_game(render& render);
    ~gfx_game();

    void on_action() override;
    void simulate() override;
    void on_output() override;

private:
    render&       render_;
    SDL_Window*   window_  = nullptr;
    SDL_Renderer* renderer_ = nullptr;
};
