#pragma once

#include <cstdint>
#include <vector>

#include "color.hpp"
#include "position.hpp"

class canvas : public std::vector<color>
{
public:
    canvas(int width, int height);

    int width;
    int height;

    color                 get_pixel_color(position position);
    void                  set_pixel(position position, color color_to_set);
    std::vector<position> get_line_pixel_positions(position start,
                                                   position end);
};