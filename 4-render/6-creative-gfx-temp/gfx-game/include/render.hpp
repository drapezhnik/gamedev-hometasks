#pragma once

#include "canvas.hpp"
#include "gfx_processor.hpp"
#include "position.hpp"

class render
{
public:
    render(canvas&        inner_canvas,
           gfx_processor& inner_gfx_processor,
           color          color_to_render);

    void clear_white();
    void clear_black();

    void render_line(position start, position end);
    void render_triangle(position v1, position v2, position v3);
    void render_triangles(std::vector<position> vertexes,
                          std::vector<uint8_t>  indexes);
    void render_gradient_triangles(std::vector<vertex>   vertexes,
                                   std::vector<uint16_t> indexes);

    std::vector<vertex> rasterize_line(vertex start, vertex end);
    std::vector<vertex> rasterize_triangle(vertex v1, vertex v2, vertex v3);

private:
    canvas        canvas_;
    gfx_processor gfx_processor_;
    color          color_;

    double interpolate(double a, double b, double t);
    vertex interpolate(vertex v1, vertex v2, double t);
};