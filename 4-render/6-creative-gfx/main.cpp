#include <cmath>
#include <iostream>

#include "SDL.h"
#include "include/render.hpp"
#include "include/twist_gfx_processor.hpp"
#include "include/window_renderer.hpp"

int main(int, char**)
{
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        std::cerr << SDL_GetError() << std::endl;
        return EXIT_FAILURE;
    }

    const int width  = 320;
    const int height = 240;

    canvas                canvas_to_render(width, height);
    twist_gfx_processor   twist_gfx_proc;
    render                render;
    window_renderer       window_renderer(canvas_to_render);

    render.set_canvas(&canvas_to_render);
    render.set_gfx_processor(&twist_gfx_proc);
    render.set_color_to_render({ 255, 0, 0 });

    render.clear_white();
    render.render_grid(10);

    auto texture = new canvas(*render.get_canvas());

    bool run    = true;
    int  result = EXIT_SUCCESS;
    while (run)
    {
        SDL_Event e;
        while (SDL_PollEvent(&e))
        {
            if (e.type == SDL_QUIT)
            {
                run = false;
                break;
            }

            // clear window
            render.clear_white();

            // set uniforms
            double mouse_x = e.motion.x;
            double mouse_y = e.motion.y;
            double radius  = 40;

            twist_gfx_proc.set_uniforms(
                { 0, 0, mouse_x, mouse_y, radius, texture });

            // calculate new canvas
            render.render_processed_canvas();

            // output
            result = window_renderer.render_in_window(canvas_to_render);
        }
    }

    delete texture;
    SDL_Quit();

    return result;
}