class vertex
{
public:
    double f0 = 0; /// x
    double f1 = 0; /// y
    double f2 = 0; /// r
    double f3 = 0; /// g
    double f4 = 0; /// b
    double f5 = 0; /// u (normalized texture coordinate)
    double f6 = 0; /// v (normalized texture coordinate)
};