#pragma once

#include "igfx_processor.hpp"

class twist_gfx_processor : public igfx_processor
{
public:
    ~twist_gfx_processor(){};

    vertex vertex_shader(vertex vertex) override;
    color  fragment_shader(vertex vertex) override;
    void   set_uniforms(uniforms u) override;

private:
    uniforms uniforms_;
};