class uniforms
{
public:
    size_t  f0 = 0;  /// canvas width
    size_t  f1 = 0;  /// canvas height
    double  f2 = 0;  /// mouse x
    double  f3 = 0;  /// mouse y
    double  f4 = 0;  /// radius
    canvas* texture; /// texture;
};