#include <algorithm>

#include "../include/render.hpp"

render::render(class canvas& canvas): canvas(canvas) {}

void render::render_line(position start, position end)
{
    color color_to_render  = { 255, 0, 0 };
    auto  pixels_to_render = canvas.get_line_pixel_positions(start, end);

    std::for_each(pixels_to_render.begin(),
                  pixels_to_render.end(),
                  [&](auto& position) { canvas.set_pixel(position, color_to_render); });
}

void render::clear_white()
{
    color white { color{ 255, 255, 255 } };
    std::fill(canvas.begin(), canvas.end(), white);
}

void render::clear_black()
{
    color black { color{ 0, 0, 0 } };
    std::fill(canvas.begin(), canvas.end(), black);
}