#include "../include/canvas.hpp"

canvas::canvas(int width, int height)
    : width(width)
    , height(height)
{
    resize(width * height);
}

color canvas::get_pixel_color(position position)
{
    int   linear_index = width * position.y * position.x;
    color& pixel        = at(linear_index);

    return pixel;
}

void canvas::set_pixel(position position, color color_to_set)
{
    int  linear_index = width * position.y + position.x;
    color& pixel_to_set = at(linear_index);
    pixel_to_set      = color_to_set;
}

std::vector<position> canvas::get_line_pixel_positions(position start,
                                                       position end)
{
    std::vector<position> line_pixels;

    // y(x) = (y2-y1)/(x2-x1)*(x-x1)+y1
    // y(x+1) = (y2-y1)/(x2-x1)*((x-x1)+1)+y1 = (y2-y1)/(x2-x1)*(x-x1)+y1  +  [(Y2-Y1)/(X2-X1)]

    int x1 = start.x;
    int y1 = start.y;
    int x2 = end.x;
    int y2 = end.y;

    int dy = std::abs(y2 - y1);
    int dx = std::abs(x2 - x1);
    int err = 0;     // float err = 0;
    int derr = (dy + 1);
    int k = y2 - y1;
    k = k > 0 ? 1 : -1;

    for(int x = x1, y = y1; x <= x2; x++)
    {
        line_pixels.push_back({ x, y });
        err += derr;

        if(err >= dx + 1)
        {
            y += k;
            err -= dx + 1;
        }
    }

    return line_pixels;
}
