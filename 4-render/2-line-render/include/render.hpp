#pragma once

#include "canvas.hpp"

class render
{
public:
    render(canvas& canvas);

    void render_line(position start, position end);
    void clear_white();
    void clear_black();

private:
    canvas& canvas;
};