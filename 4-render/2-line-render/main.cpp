#include "include/render.hpp"
#include "include/canvas.hpp"
#include "include/image_manager.hpp"

int main(int, char**)
{
    canvas canvas(800,600);
    render render(canvas);
    image_manager image_manager;

    render.clear_white();
    render.render_line({0,300}, {799,400});
    image_manager.save_image(canvas);
}