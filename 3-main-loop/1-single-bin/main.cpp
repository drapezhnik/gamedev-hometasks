#include <iostream>
#include <map>
#include <string_view>
#include <algorithm>

#include "SDL.h"

class KeyboardMapping{
public:
    SDL_Keycode key_code;
    std::string action_name;
    bool is_pressed;
};


void process_input(SDL_Event& event, KeyboardMapping& input)
{
    const static std::map<SDL_Keycode, std::string_view> mappings =
    {
        { SDLK_w, "move forward" },
        { SDLK_a, "move left" },
        { SDLK_s, "move back" },
        { SDLK_d, "move right" },
        { SDLK_UP, "move forward" },
        { SDLK_LEFT, "move left" },
        { SDLK_DOWN, "move back" },
        { SDLK_RIGHT, "move right" },
        { SDLK_LCTRL, "crouch" },
        { SDLK_SPACE, "jump" },
        { SDLK_ESCAPE, "quit" }
    };

    auto action = mappings.find(static_cast<SDL_Keycode>(event.key.keysym.sym));

    if (action == std::end(mappings))
    {
        input.key_code = SDLK_UNKNOWN;
        input.action_name = "unknown";
        input.is_pressed = false;
        return;
    }

    input.key_code = action->first;
    input.action_name = action->second;

    switch (event.type)
    {
        case SDL_KEYDOWN:
            input.is_pressed = true;
            break;
        case SDL_KEYUP:
            input.is_pressed = false;
            break;
    }
}

void resolve_output(KeyboardMapping& input)
{
    if(input.action_name != "unknown")
    {
        std::string output = input.is_pressed
                                      ? input.action_name + " is pressed"
                                      : input.action_name + " is released";

        std::cout << output << std::endl;
    }
}

int main(int, char**)
{
    SDL_Event event;
    SDL_Window* window;
    KeyboardMapping input;

    window = SDL_CreateWindow(
        "Loop as single bin",
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        800, 600,
        0);

    bool run = true;
    while(run)
    {
        while(SDL_PollEvent(&event))
        {
            process_input(event, input);

            if(input.action_name == "quit" || event.type == SDL_QUIT)
            {
                run = false;
            }

            resolve_output(input);
        }
    }

    SDL_DestroyWindow(window);
    SDL_Quit();

    return EXIT_SUCCESS;
}