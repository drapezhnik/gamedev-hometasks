cmake_minimum_required(VERSION 3.17)

project(main-loop-hot-reload CXX)

set(CMAKE_CXX_STANDARD 17)

add_executable(engine-exe
        main.cpp
        Engine/Engine.hpp
        Engine/Engine.cpp
        Engine/HotLoader.hpp
        Engine/HotLoader.cpp)

set_target_properties(engine-exe PROPERTIES
        ENABLE_EXPORTS 1)

add_library(game-lib SHARED
        Game/Game.cpp)

find_package(SDL2 REQUIRED)

target_link_libraries(engine-exe PRIVATE
        SDL2::SDL2)
target_link_libraries(game-lib PRIVATE
        engine-exe)