#include <chrono>
#include <string>

#pragma once

class Game;

class HotLoader
{
public:
    explicit HotLoader(std::string& libName);
    bool  IsGameUpdated();
    Game* LoadGame();
    void  UnloadGame();

private:
    Game*       _old          = nullptr;
    const char* _libName      = nullptr;
    const char* _tmpLibName   = nullptr;
    void*       _tmpLibHandle = nullptr;
    std::time_t _libLastUpdateTime;
};
