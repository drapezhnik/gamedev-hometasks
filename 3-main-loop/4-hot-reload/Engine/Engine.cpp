#include <iostream>
#include <map>

#include "Engine.hpp"
#include "SDL.h"

class EngineSDL : public Engine
{
public:
    bool Init() override
    {
        _window = SDL_CreateWindow("Loop with separated engine",
                                  SDL_WINDOWPOS_CENTERED,
                                  SDL_WINDOWPOS_CENTERED,
                                  800,
                                  600,
                                  0);

        if(_window == nullptr)
        {
            std::cout << "Error: engine init failed" << std::endl;
            return false;
        }

        return true;
    };

    void ShutDown() override
    {
        SDL_DestroyWindow(_window);
        SDL_Quit();
    }

    bool ProcessInput(Action& action) override
    {
        // map SDL keycodes to engine's actions
        const static std::map<SDL_Keycode, ActionName>
            sdl_to_engine_mappings = {
                { SDLK_w, ActionName::UP },       { SDLK_a, ActionName::LEFT },
                { SDLK_s, ActionName::BACK },     { SDLK_d, ActionName::RIGHT },
                { SDLK_i, ActionName::BTN1 },     { SDLK_j, ActionName::BTN2 },
                { SDLK_k, ActionName::BTN3 },     { SDLK_l, ActionName::BTN4 },
                { SDLK_ESCAPE, ActionName::QUIT }
            };

        // get SDL event
        static SDL_Event event; // TODO: test static
        SDL_PollEvent(&event);

        // quit on dedicated SDL event
        if (event.type == SDL_QUIT)
        {
            return false;
        }

        // we are not interested in other types of events for now
        if(event.type != SDL_KEYDOWN && event.type != SDL_KEYUP)
        {
            return true;
        }

        // look for engine's action name by SDL keycode
        auto mapping = sdl_to_engine_mappings.find(event.key.keysym.sym);

        // return if not found, but program continues running
        if (mapping == std::end(sdl_to_engine_mappings))
        {
            action.action_name = ActionName::UNKNOWN;
            return true;
        }

        // quit if dedicated key is pressed
        if (action.action_name == ActionName::QUIT)
        {
            return false;
        }

        // store engine's action name for future output
        action.action_name = mapping->second;

        // determine key state
        switch (event.type)
        {
            case SDL_KEYDOWN:
                action.pressed = true;
                break;
            case SDL_KEYUP:
                action.pressed = false;
                break;
        }

        return true;
    };

private:
    SDL_Window* _window = nullptr;
};

Engine* ResolveEngine()
{
    Engine* engine = new EngineSDL;
    return engine;
};

void DestructEngine(Engine* engine)
{
    delete engine;
};

Engine::~Engine(){};
