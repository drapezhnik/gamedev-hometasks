#include <filesystem>
#include <iostream>
#include <vector>

#include "Engine.hpp"
#include "HotLoader.hpp"
#include "SDL.h"

HotLoader::HotLoader(std::string& libName)
    : _libName((new std::string("./lib" + libName + ".dylib"))->c_str())
    , _tmpLibName((new std::string("./tmplib" + libName + ".dylib"))->c_str())
{
}

bool HotLoader::IsGameUpdated()
{
    try
    {
        auto libCurrentUpdateTime = std::filesystem::last_write_time(_libName);

        std::time_t libCurrentUpdateTime_ = decltype(libCurrentUpdateTime)::clock::to_time_t(libCurrentUpdateTime);

        if (libCurrentUpdateTime_ != _libLastUpdateTime)
        {
            _libLastUpdateTime = libCurrentUpdateTime_;
            return true;
        }

    }
    catch(std::exception& ex)
    {
        std::cout << ex.what();
    }

    return false;
}

Game* HotLoader::LoadGame()
{
    // unload tmp lib
    if (!_old)
    {
        SDL_UnloadObject(_tmpLibHandle);
    }

    // remove tmp lib
    if (std::filesystem::exists(_tmpLibName))
    {
        if (!std::filesystem::remove(_tmpLibName))
        {
            std::cerr << "Error: can't remove " << _tmpLibName << std::endl;
            return nullptr;
        }
    }

    // copy tmp lib
    try
    {
        std::filesystem::copy(_libName, _tmpLibName);
    }
    catch (const std::exception& e)
    {
        std::cerr << "Error: can't copy [" << _libName << "] to ["
                  << _tmpLibName << ']' << std::endl;
        return nullptr;
    }

    // load tmp lib
    void* newTmpLibHandle = SDL_LoadObject(_tmpLibName);

    if (newTmpLibHandle == nullptr)
    {
        std::cerr << "Error: failed to load [" << _tmpLibName << ']'
                  << SDL_GetError() << std::endl;
        return nullptr;
    }

    _tmpLibHandle = newTmpLibHandle;

    void* _createGameLibFnPtr = SDL_LoadFunction(_tmpLibHandle, "CreateGame");

    if (_createGameLibFnPtr == nullptr)
    {
        std::cerr << "Error: no function [CreateGame] in [" << _tmpLibName
                  << ']' << std::endl;
        return nullptr;
    }

    typedef decltype(&CreateGame) CreateGameFn;

    auto createGameFnPtrLocal =
        reinterpret_cast<CreateGameFn>(_createGameLibFnPtr);

    Game* _old = createGameFnPtrLocal();

    if (_old == nullptr)
    {
        std::cerr << "Error: function [CreateGame] returned nullptr"
                  << std::endl;
        return nullptr;
    }

    return _old;
}

void HotLoader::UnloadGame()
{
    void* _destructGameLibFnPtr =
        SDL_LoadFunction(_tmpLibHandle, "DestructGame");

    if (_destructGameLibFnPtr == nullptr)
    {
        std::cerr << "Error: no function [DestructGame] in [" << _tmpLibName
                  << ']' << std::endl;
        return;
    }

    typedef decltype(&DestructGame) DestructGameFn;

    auto destructGameFnPtrLocal =
        reinterpret_cast<DestructGameFn>(_destructGameLibFnPtr);

    destructGameFnPtrLocal();
}
