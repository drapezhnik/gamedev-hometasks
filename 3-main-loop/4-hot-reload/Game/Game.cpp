#include <iostream>
#include <map>
#include <string>
#include <thread>

#include "../Engine/Engine.hpp"

class ConsoleOutputterGame : public Game
{
public:
    ConsoleOutputterGame()
    {
        _engineToGameMappings = {
            { ActionName::UP, "up" },         { ActionName::LEFT, "left" },
            { ActionName::BACK, "back" },     { ActionName::RIGHT, "right" },
            { ActionName::BTN1, "button 1" }, { ActionName::BTN2, "button 2" },
            { ActionName::BTN3, "button 3" }, { ActionName::BTN4, "button 4" }
        };
    }

    void OnAction(Action& action) override
    {
        auto mapping = _engineToGameMappings.find(action.action_name);

        _actionName = mapping == std::end(_engineToGameMappings)
                          ? "nothing"
                          : mapping->second;

        _state = action.pressed ? "pressed" : "released";
    }

    void Simulate() override
    {
        _toRender = _actionName == "nothing" ? std::string()
                                             : _actionName + " is " + _state;
        std::this_thread::sleep_for(std::chrono::microseconds(30));
    }

    void Render() override
    {
        std::cout << _toRender << std::endl;
    }

private:
    std::map<ActionName, std::string_view> _engineToGameMappings;
    std::string                            _actionName;
    std::string                            _state;
    std::string                            _toRender;
};

Game* CreateGame()
{
    auto game = new ConsoleOutputterGame();
    return game;
}

void DestructGame(Game* game)
{
    delete game;
}
