#pragma once

enum class ActionName
{
    UNKNOWN,
    QUIT,
    UP,
    LEFT,
    BACK,
    RIGHT,
    BTN1,
    BTN2,
    BTN3,
    BTN4
};

class Action
{
public:
    ActionName action_name;
    bool pressed;
};

class Engine
{
public:
    virtual ~Engine();
    virtual void Init() = 0;
    virtual void ShutDown() = 0;
    virtual bool ProcessInput(Action& action) = 0;
    virtual void SetOutput(Action& action) = 0;
};

Engine* ResolveEngine();
void DestructEngine(Engine* engine);